#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <tuple>
using namespace std;

struct ListNode {
  int       val;
  ListNode* next;

  ListNode() : val(0), next(nullptr) {}

  ListNode(int x) : val(x), next(nullptr) {}

  ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:
  ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
    ListNode* result    = nullptr;
    int       remaining = 0;
    while (l1 != nullptr or l2 != nullptr) {
      std::tie(remaining, result) = addSumOfNodesToResult(l1, l2, result, remaining);
      l1                          = l1 != nullptr ? l1->next : nullptr;
      l2                          = l2 != nullptr ? l2->next : nullptr;
    }
    if (0 != remaining) { result = new ListNode(remaining, result); }
    return reverseList(result);
  }

private:
  std::tuple<int, ListNode*> addSumOfNodesToResult(ListNode* n1, ListNode* n2, ListNode* result,
                                                   int prevRemaining) {
    const int n1Val          = n1 != nullptr ? n1->val : 0;
    const int n2Val          = n2 != nullptr ? n2->val : 0;
    int       newVal         = n1Val + n2Val + prevRemaining;
    int       nextRemaining  = newVal / 10;
    newVal                  %= 10;
    return {nextRemaining, new ListNode(newVal, result)};
  }

  ListNode* reverseList(ListNode* l) {
    ListNode* prevNode   = nullptr;
    ListNode* actualNode = l;
    while (actualNode != nullptr) {
      ListNode* nextNode = actualNode->next;
      actualNode->next   = prevNode;
      prevNode           = actualNode;
      actualNode         = nextNode;
    }
    return prevNode;
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
