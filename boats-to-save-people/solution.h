#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <algorithm>
#include <vector>

using namespace std;

class Solution {
public:
  int numRescueBoats(vector<int>& people, int limit) {
    std::sort(people.begin(), people.end());
    int leftIndex  = 0;
    int rightIndex = people.size() - 1;
    int nBoats     = 0;
    while (leftIndex < rightIndex) {
      if (people[leftIndex] + people[rightIndex] > limit) {
        --rightIndex;
      } else {
        ++leftIndex;
        --rightIndex;
      }
      ++nBoats;
    }
    if (leftIndex == rightIndex) { ++nBoats; }
    return nBoats;
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
