#include <gtest/gtest.h>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<vector<int>, int, int>> {
protected:
  static string vectorToString(const vector<int>& v) {
    string result;
    for (auto value : v) { result += to_string(value) + ", "; }
    if (!result.empty()) {
      result.pop_back();
      result.pop_back();
    }
    return result;
  }

  void SetUp() override {
    people   = std::get<0>(GetParam());
    limit    = std::get<1>(GetParam());
    expected = std::get<2>(GetParam());
  }

  Solution    solution;
  vector<int> people;
  int         limit;
  int         result;
  int         expected;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.numRescueBoats(people, limit);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(Tests, SUITE_NAME,
                         ::testing::Values(std::make_tuple(vector<int>({1, 2}), 3, 1),
                                           std::make_tuple(vector<int>({3, 2, 2, 1}), 3, 3),
                                           std::make_tuple(vector<int>({3, 5, 3, 4}), 5, 4)));
