#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <array>
#include <string>

using namespace std;

class Solution {
public:
  bool buddyStrings(const string& s, const string& goal) {
    const auto inputStringSize = s.size();
    if (inputStringSize != goal.size() || inputStringSize == 1) { return false; }
    if (s == goal) { return Solution::twoEqualLetters(s); }
    int64_t firstDifferenceIndex = -1;
    bool    alreadySwapped       = false;
    for (size_t i = 0; i < s.size(); ++i) {
      if (s[i] != goal[i]) {
        if (alreadySwapped) { return false; }
        if (firstDifferenceIndex != -1) {
          if (s[i] != goal[firstDifferenceIndex] or s[firstDifferenceIndex] != goal[i]) {
            return false;
          }
          alreadySwapped = true;
        } else {
          firstDifferenceIndex = i;
        }
      }
    }
    return alreadySwapped;
  }

private:
  static bool twoEqualLetters(const string& s) {
    std::array<char, 'z' - 'a' + 1> foundLetters{0};
    for (const char i : s) {
      if (++foundLetters[i - 'a'] == 2) { return true; }
    }
    return false;
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
