#include <gtest/gtest.h>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<string, string, bool>> {
protected:
  void SetUp() override {
    input    = std::get<0>(GetParam());
    goal     = std::get<1>(GetParam());
    expected = std::get<2>(GetParam());
  }

  Solution solution;
  string   input;
  string   goal;
  bool     expected;
  bool     result;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.buddyStrings(input, goal);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(Tests, SUITE_NAME,
                        ::testing::Values(std::make_tuple("ba", "ab", true),
                                          std::make_tuple("ab", "ab", false),
                                          std::make_tuple("aa", "aa", true),
                                          std::make_tuple("abcaa", "abcbb", false),
                                          std::make_tuple("abac", "abad", false)));
