#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <condition_variable>
#include <functional>
#include <semaphore>

using namespace std;

class H2O {
public:
  H2O() = default;

  void hydrogen(function<void()> releaseHydrogen) {
    hSmph_.acquire();
    // std::cout << "<<H" << std::endl;
    arrive_and_wait(nHydrogen_);
    // cout << ">>H" << endl;
    releaseHydrogen();
    hSmph_.release();
  }

  void oxygen(function<void()> releaseOxygen) {
    oSmph_.acquire();
    // std::cout << "<<O" << std::endl;
    arrive_and_wait(nOxygen_);
    // cout << ">>O" << endl;
    releaseOxygen();
    oSmph_.release();
  }

private:
  inline void arrive_and_wait(int& molecule) {
    std::unique_lock lg(m_);
    ++molecule;
    if (isComplete()) {
      // cout << "reset & notify" << endl;
      reset();
      cv_.notify_all();
    } else {
      const int localIteration = iteration;
      // cout << "enter waiting" << endl;
      cv_.wait(lg, [this, localIteration]() { return iteration != localIteration; });
      // cout << "end waiting" << endl;
    }
  }

  inline bool isComplete() const {
    // std::cout << "H*" << nHydrogen_ << "-O*" << nOxygen_ << std::endl;
    return nHydrogen_ >= 2 && nOxygen_ >= 1;
  }

  inline void reset() {
    nHydrogen_ = 0;
    nOxygen_   = 0;
    ++iteration;
  }

  int                     nHydrogen_ = 0;
  int                     nOxygen_   = 0;
  int                     iteration  = 0;
  std::condition_variable cv_;
  std::mutex              m_;
  counting_semaphore<2>   hSmph_{2};
  counting_semaphore<1>   oSmph_{1};
};

using Solution = H2O;

#endif  // LEETCODEEXERCISES_SOLUTION_H
