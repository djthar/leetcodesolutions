#include <gtest/gtest.h>

#include <future>
#include <unordered_set>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<string>> {
protected:
  void SetUp() override {
    input    = std::get<0>(GetParam());
    solution = std::make_unique<Solution>();
  }

  std::unique_ptr<Solution> solution;
  string                    input;
  string                    result;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  const auto                     releaseHydrogen = [this]() { result.append("H"); };
  const auto                     releaseOxygen   = [this]() { result.append("O"); };

  std::vector<std::future<void>> threads;
  threads.reserve(input.size());
  for (char l : input) {
    if (l == 'H') {
      threads.emplace_back(std::async(
          std::launch::async, [this, &releaseHydrogen]() { solution->hydrogen(releaseHydrogen); }));
    } else {
      threads.emplace_back(std::async(
          std::launch::async, [this, &releaseOxygen]() { solution->oxygen(releaseOxygen); }));
    }
  }

  for (auto& thread : threads) { thread.wait(); }
  for (size_t i = 0; i < (result.size() / 3); ++i) {
    int hCount = 0;
    int oCount = 0;
    for (size_t j = 0; j < 3; ++j) {
      if (result[i * 3 + j] == 'H') {
        ++hCount;
      } else {
        ++oCount;
      }
    }
    EXPECT_EQ(hCount, 2);
    EXPECT_EQ(oCount, 1);
  }
}

INSTANTIATE_TEST_SUITE_P(Tests, SUITE_NAME,
                        ::testing::Values(std::make_tuple("HOH"), std::make_tuple("HHHOOH"),
                                          std::make_tuple("OOHHHH")));
