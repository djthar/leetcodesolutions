#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <algorithm>
#include <ranges>
#include <vector>

using namespace std;
using std::ranges::subrange;

class Solution {
public:
  bool canPlaceFlowers(vector<int>& flowerbed, int n) {
    if (0 == n) { return true; }
    for (auto it = flowerbed.begin(); it != flowerbed.end(); ++it) {
      if (plant(subrange<vector<int>::iterator>(it, min(it + 3, flowerbed.end())))) {
        --n;
        if (0 == n) { return true; }
      }
    }
    return false;
  }

  static bool plant(subrange<vector<int>::iterator>&& group) {
    if (std::ranges::all_of(group, [](int i) { return i == 0; })) {
      group[group.size() >> 1] = 1;
      return true;
    }
    return false;
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
