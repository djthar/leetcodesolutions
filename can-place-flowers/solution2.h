#ifndef LEETCODEEXERCISES_SOLUTION_2_H
#define LEETCODEEXERCISES_SOLUTION_2_H

#include <algorithm>
#include <iostream>
#include <vector>

using range = std::tuple<const std::vector<int>::iterator, const std::vector<int>::iterator>;

class Solution2 {
public:
  bool canPlaceFlowers(std::vector<int>& flowerbed, int n) {
    if (0 == n) { return true; }
    for (auto it = flowerbed.begin(); it != flowerbed.end(); ++it) {
      if (plant(getRange(it, flowerbed))) {
        --n;
        if (0 == n) { return true; }
      }
    }
    return false;
  }

  static bool plant(range r) {
    for (auto it = get<0>(r); it != get<1>(r); ++it) { std::cout << *it << " "; }
    std::cout << std::endl;
    size_t rangeSize = (get<1>(r) - get<0>(r));
    if (std::all_of(get<0>(r), get<1>(r), [](int i) { return i == 0; })) {
      std::cout << "rangeSize: " << rangeSize << "\n";
      *(get<0>(r) + (rangeSize == 3 ? 1 : 0)) = 1;
      return true;
    }
    return false;
  }

  static range getRange(const std::vector<int>::iterator& position, std::vector<int>& flowerbed) {
    return {position == flowerbed.begin() ? flowerbed.begin() : position - 1,
            position == flowerbed.end()       ? flowerbed.end()
            : position + 1 == flowerbed.end() ? position + 1
                                              : position + 2};
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION_2_H
