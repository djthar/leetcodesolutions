#include <gtest/gtest.h>

#include "solution.h"
#include "solution2.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<vector<int>, int, bool>> {
protected:
  static string vectorToString(const vector<int>& v) {
    string result;
    for (auto value : v) { result += to_string(value) + ", "; }
    if (!result.empty()) {
      result.pop_back();
      result.pop_back();
    }
    return result;
  }

  void SetUp() override {
    input    = std::get<0>(GetParam());
    target   = std::get<1>(GetParam());
    expected = std::get<2>(GetParam());
  }

  Solution2   solution;
  vector<int> input;
  int         target = 0;
  bool        result;
  bool        expected;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  cout << "=== START ===\n";
  cout << "input: " << vectorToString(input) << "\n";
  result = solution.canPlaceFlowers(input, target);
  cout << "===  END  ===\n";
  EXPECT_EQ(result, expected) << "result and expected differ for input " << vectorToString(input)
                              << " and n " << target << ". " << (result ? "true" : "false")
                              << " vs " << (expected ? "true" : "false");
}

INSTANTIATE_TEST_SUITE_P(
    Tests, SUITE_NAME,
    ::testing::Values(std::make_tuple(vector<int>({1, 0, 0, 0, 1}), 1, true),
                      std::make_tuple(vector<int>({1, 0, 0, 0, 1}), 2, false),
                      std::make_tuple(vector<int>({1, 0, 0, 0, 0}), 2, true),
                      std::make_tuple(vector<int>({0, 0, 0, 0, 1}), 2, true),
                      std::make_tuple(vector<int>({1, 0, 0, 0, 0, 1}), 2, false),
                      std::make_tuple(vector<int>({0, 0, 0, 0, 0, 1, 0, 0}), 0, true)));
