#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <array>
#include <vector>
using namespace std;

// Definition for a Node.
class Node {
public:
  int           val;
  vector<Node*> neighbors;

  Node() {
    val       = 0;
    neighbors = vector<Node*>();
  }

  explicit Node(int _val) {
    val       = _val;
    neighbors = vector<Node*>();
  }

  Node(int _val, vector<Node*> _neighbors) {
    val       = _val;
    neighbors = std::move(_neighbors);
  }
};

class Solution {
public:
  Node* cloneGraph(Node* node) {
    if (!node) { return nullptr; }
    dfs(node);
    return nodes_[0];
  }

  void dfs(Node* node) {
    auto [actualNode, _] = getOrCreateNode(node->val);
    for (auto n : node->neighbors) {
      auto [newNeighbor, isNew] = getOrCreateNode(n->val);
      actualNode->neighbors.emplace_back(newNeighbor);
      if (isNew) { dfs(n); }
    }
  }

  ~Solution() {
    for (auto node : nodes_) { delete node; }
  }

private:
  auto getOrCreateNode(int val) -> tuple<Node*, bool> {
    if (nullptr == nodes_[val - 1]) {
      nodes_[val - 1] = new Node(val);
      return {nodes_[val - 1], true};
    }
    return {nodes_[val - 1], false};
  }

  std::array<Node*, 100> nodes_{};
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
