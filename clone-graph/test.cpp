#include <gtest/gtest.h>

#include <unordered_set>

#include "solution.h"

using namespace std;

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<vector<vector<int>>>> {
protected:
  vector<vector<int>> createVector(Node* node) {
    vector<vector<int>>            r(input.size());
    std::unordered_map<int, Node*> nodes;
    unordered_set<Node*>           visited;
    dfs(node, visited, [&nodes](Node* node) { nodes.emplace(node->val, node); });
    for (int i = 1; i <= input.size(); ++i) {
      const auto actualNode = nodes[i];
      if (nullptr == actualNode) { continue; }
      r[i - 1].reserve(actualNode->neighbors.size());
      for (const auto n : actualNode->neighbors) { r[i - 1].emplace_back(n->val); }
    }
    return r;
  }

  void dfs(Node* node, unordered_set<Node*>& visited, std::function<void(Node*)> fn) {
    visited.emplace(node);
    fn(node);
    for (auto n : node->neighbors) {
      if (!visited.contains(n)) { dfs(n, visited, fn); }
    }
  }

  static string vectorToString(const vector<int>& v) {
    string result;
    for (auto value : v) { result += to_string(value) + ", "; }
    if (!result.empty()) {
      result.pop_back();
      result.pop_back();
    }
    return result;
  }

  void SetUp() override {
    input = std::get<0>(GetParam());
    nodes_.resize(input.size());
    for (int i = 0; i < input.size(); ++i) { nodes_[i] = make_unique<Node>(i + 1); }
    for (int i = 0; i < input.size(); ++i) {
      nodes_[i]->neighbors.reserve(input[i].size());
      for (auto n : input[i]) { nodes_[i]->neighbors.emplace_back(nodes_[n - 1].get()); }
    }
  }

  Solution                 solution;
  vector<unique_ptr<Node>> nodes_;
  vector<vector<int>>      input;
  Node*                    result = nullptr;
  vector<int>              expected;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result       = solution.cloneGraph(nodes_[0].get());
  auto resultV = createVector(result);
  EXPECT_EQ(resultV, input);

  /*
  unordered_set<Node*> visited;
  dfs(result, visited, [](Node*){});
  for(auto n : visited) {
      delete n;
  }
   */
}

INSTANTIATE_TEST_SUITE_P(Tests, SUITE_NAME,
                        ::testing::Values(std::make_tuple(vector<vector<int>>({
                          {2, 4},
                          {1, 3},
                          {2, 4},
                          {1, 3}
}))));
