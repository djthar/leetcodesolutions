#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <algorithm>
#include <vector>

using namespace std;

class Solution {
public:
  void combinationSumRecursive(vector<int>::const_iterator init, vector<int>::const_iterator last,
                               vector<int> accumulated, int target) {
    if (init == last) { return; }
    auto lastViableElement = upper_bound(init, last, target);
    if (lastViableElement != last) { last = lastViableElement; }
    const int currentValue = *init;
    if (target < currentValue) { return; }
    if (target == currentValue) {
      result.emplace_back(copyAndAppend(accumulated, currentValue));
    } else {
      combinationSumRecursive(init, last, copyAndAppend(accumulated, currentValue),
                              target - currentValue);
    }
    combinationSumRecursive(init + 1, last, move(accumulated), target);
  }

  vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
    sort(candidates.begin(), candidates.end());
    combinationSumRecursive(candidates.begin(), candidates.end(), {}, target);
    return result;
  }

private:
  static vector<int> copyAndAppend(const vector<int>& original, int newElement) {
    vector<int> result;
    result.reserve(original.size() + 1);
    result.insert(end(result), begin(original), end(original));
    result.emplace_back(newElement);
    return result;
  }

  vector<vector<int>> result;
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
