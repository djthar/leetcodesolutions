#include <gtest/gtest.h>

#include <unordered_set>

#include "solution.h"

class SUITE_NAME :
    public ::testing::TestWithParam<std::tuple<vector<int>, int, vector<vector<int>>>> {
protected:
  void SetUp() override {
    input    = std::get<0>(GetParam());
    target   = std::get<1>(GetParam());
    expected = std::get<2>(GetParam());
  }

  Solution            solution;
  vector<int>         input;
  int                 target = 0;
  vector<vector<int>> expected;
  vector<vector<int>> result;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.combinationSum(input, target);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(
    Tests, SUITE_NAME,
    ::testing::Values(std::make_tuple(vector<int>({
                                        2, 3, 6, 7
}),
                                      7, vector<vector<int>>({{2, 2, 3}, {7}})),
                      std::make_tuple(vector<int>({2, 3, 5}), 8,
                                      vector<vector<int>>({{2, 2, 2, 2}, {2, 3, 3}, {3, 5}})),
                      std::make_tuple(vector<int>({2}), 1, vector<vector<int>>()),
                      std::make_tuple(vector<int>({1}), 1, vector<vector<int>>({{1}})),
                      std::make_tuple(vector<int>({1}), 2, vector<vector<int>>({{1, 1}}))));
