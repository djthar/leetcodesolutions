#include <gtest/gtest.h>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<vector<int>, int>> {
protected:
  void SetUp() override {
    input    = std::get<0>(GetParam());
    expected = std::get<1>(GetParam());
  }

  Solution         solution;
  std::vector<int> input;
  int              expected = 0;
  int              result   = 0;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.maxArea(input);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(Tests, SUITE_NAME,
                        ::testing::Values(std::make_tuple(vector<int>({1, 8, 6, 2, 5, 4, 8, 3, 7}),
                                                          49),
                                          std::make_tuple(vector<int>({1, 1}), 1),
                                          std::make_tuple(vector<int>({4, 3, 2, 1, 4}), 16),
                                          std::make_tuple(vector<int>({1, 2, 1}), 2)));
