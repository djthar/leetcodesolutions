#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <optional>
#include <vector>

using namespace std;

struct TreeNode {
  int       val;
  TreeNode* left;
  TreeNode* right;

  TreeNode() : val(0), left(nullptr), right(nullptr) {}

  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}

  TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};

class Solution {
  using Range_t        = tuple<size_t, size_t>;
  using RangeTriplet_t = tuple<optional<Range_t>, std::size_t, optional<Range_t>>;

public:
  TreeNode* sortedArrayToBST(vector<int>& nums) {
    nums_ = &nums;
    return sortedArrayToBSTRecursive(Range_t(0, nums.size() - 1));
  }

private:
  TreeNode* sortedArrayToBSTRecursive(optional<Range_t> range) {
    if (!range) { return nullptr; }
    const RangeTriplet_t splittedRange = splitRange(range.value());
    return new TreeNode((*nums_)[get<1>(splittedRange)],
                        sortedArrayToBSTRecursive(get<0>(splittedRange)),
                        sortedArrayToBSTRecursive(get<2>(splittedRange)));
  }

  static RangeTriplet_t splitRange(const Range_t& range) {
    const size_t      middlePoint = getRangeMiddlePoint(range);
    optional<Range_t> leftSide    = nullopt;
    optional<Range_t> rightSide   = nullopt;
    if (middlePoint > get<0>(range)) { leftSide = Range_t(get<0>(range), middlePoint - 1); }
    if (middlePoint < get<1>(range)) { rightSide = Range_t(middlePoint + 1, get<1>(range)); }
    return {leftSide, middlePoint, rightSide};
  }

  static size_t getRangeMiddlePoint(const Range_t& range) {
    return (get<0>(range) + get<1>(range)) / 2;
  }

  vector<int>* nums_;
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
