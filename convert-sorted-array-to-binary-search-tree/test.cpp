#include <gtest/gtest.h>

#include <functional>
#include <queue>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<vector<int>, vector<optional<int>>>> {
protected:
  void SetUp() override {
    input    = std::get<0>(GetParam());
    expected = std::get<1>(GetParam());
  }

  void TearDown() override { freeTree(result); }

  void freeTree(TreeNode*& node) {
    if (nullptr == node) { return; }
    freeTree(node->left);
    freeTree(node->right);
    delete node;
    node = nullptr;
  }

  [[nodiscard]] vector<optional<int>> getComparableResult() const {
    vector<optional<int>> comparableResult;
    queue<TreeNode*>      nodes;
    nodes.push(result);
    while (not nodes.empty()) {
      TreeNode* node = nodes.front();
      nodes.pop();
      if (nullptr != node) {
        comparableResult.emplace_back(node->val);
        if (nullptr != node->left or nullptr != node->right) {
          nodes.push(node->left);
          nodes.push(node->right);
        }
      } else {
        comparableResult.emplace_back(nullopt);
      }
    }
    return comparableResult;
  }

  Solution              solution;
  vector<int>           input;
  vector<optional<int>> expected;
  TreeNode*             result = nullptr;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result                                 = solution.sortedArrayToBST(input);
  vector<optional<int>> comparableResult = getComparableResult();
  EXPECT_EQ(comparableResult, expected);
}

INSTANTIATE_TEST_SUITE_P(Tests, SUITE_NAME,
                        ::testing::Values(std::make_tuple(vector<int>({-10, -3, 0, 5, 9}),
                                                          vector<optional<int>>({0, -10, 5, nullopt,
                                                                                 -3, nullopt, 9})),
                                          std::make_tuple(vector<int>({1, 3}),
                                                          vector<optional<int>>({1, nullopt, 3}))));
