#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <algorithm>
#include <string>
#include <vector>

using namespace std;

class Solution {
  using Numbers = std::vector<size_t>;

public:
  string countAndSay(int n) {
    if (1 == n) { return "1"; }
    Numbers numbers = {1};
    for (size_t i = 2; i <= n; ++i) { numbers = say(numbers); }
    string result;
    std::transform(numbers.begin(), numbers.end(), std::back_inserter(result),
                   [](size_t c) -> char { return static_cast<char>('0' + c); });
    return result;
  }

private:
  Numbers say(const Numbers& numbers) {
    Numbers result;
    size_t  count = 1;
    for (size_t i = 1; i < numbers.size(); ++i) {
      if (numbers[i] == numbers[i - 1]) {
        ++count;
      } else {
        result.emplace_back(count);
        result.emplace_back(numbers[i - 1]);
        count = 1;
      }
    }
    if (0 != count) {
      result.emplace_back(count);
      result.emplace_back(numbers.back());
    }
    return result;
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
