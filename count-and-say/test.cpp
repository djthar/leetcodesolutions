#include <gtest/gtest.h>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<int, string>> {
protected:
  void SetUp() override {
    input    = std::get<0>(GetParam());
    expected = std::get<1>(GetParam());
  }

  Solution solution;
  int      input = 0;
  string   expected;
  string   result;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.countAndSay(input);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(Tests, SUITE_NAME,
                        ::testing::Values(std::make_tuple(6, "312211"),
                                          std::make_tuple(5, "111221"), std::make_tuple(4, "1211"),
                                          std::make_tuple(3, "21"), std::make_tuple(2, "11"),
                                          std::make_tuple(1, "1")));
