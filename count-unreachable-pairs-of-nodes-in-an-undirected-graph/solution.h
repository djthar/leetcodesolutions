#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <deque>
#include <unordered_set>
#include <vector>

using namespace std;

class Node {
public:
  std::unordered_set<int> childs;
  bool                    visited = false;
};

class Solution {
public:
  long long countPairs(int n, vector<vector<int>>& edges) {
    nodes_.resize(n);
    for (const auto& connection : edges) {
      auto& node1 = nodes_[connection[0]];
      node1.childs.emplace(connection[1]);
      auto& node2 = nodes_[connection[1]];
      node2.childs.emplace(connection[0]);
    }

    int              unconnectedGroups = 0;
    std::vector<int> nNodes;
    for (auto& node : nodes_) {
      if (!node.visited) {
        ++unconnectedGroups;
        nNodes.emplace_back(visitBfs(node));
      }
    }
    auto nNodesSum = nNodes;
    for (auto it = nNodesSum.rbegin() + 1; it != nNodesSum.rend(); ++it) { *it += *(it - 1); }
    int result = 0;
    for (size_t i = 0; i < nNodes.size() - 1; ++i) { result += nNodes[i] * nNodesSum[i + 1]; }
    return result;
  }

private:
  void visit(Node& node) {
    node.visited = true;
    for (int childIdx : node.childs) {
      Node& childNode = nodes_[childIdx];
      if (!childNode.visited) { visit(childNode); }
    }
  }

  int visitBfs(Node& node) {
    node.visited = true;
    deque<Node*> nodes;
    nodes.emplace_back(&node);
    int nNodes = 1;
    while (!nodes.empty()) {
      auto pNode = nodes.front();
      nodes.pop_front();
      for (auto childIdx : pNode->childs) {
        Node& childNode = nodes_[childIdx];
        if (!childNode.visited) {
          nodes.push_back(&childNode);
          childNode.visited = true;
          ++nNodes;
        }
      }
    }
    return nNodes;
  }

  std::vector<Node> nodes_;
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
