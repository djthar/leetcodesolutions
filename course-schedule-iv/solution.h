#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <cstdlib>
#include <unordered_set>
#include <vector>
using namespace std;

class Solution {
  class Node {
  public:
    void addPrerequisite(Node* prerequisite) {
      auto [it, inserted] = prerequisites_.emplace(prerequisite);
      if (not inserted) { return; }
      for (auto p : prerequisite->prerequisites_) { addPrerequisite(p); }
      prerequisite->parents_.emplace(this);
      for (auto parent : parents_) { parent->addPrerequisite(prerequisite); }
      // std::cout << "adding to " << index_ << " the prerequisite " << prerequisite->index_ <<
      // std::endl;
    }

    bool isPrerequisite(Node* prerequisite) { return prerequisites_.count(prerequisite) != 0; }

  private:
    int                       index_;
    std::unordered_set<Node*> prerequisites_;
    std::unordered_set<Node*> parents_;
  };

public:
  vector<bool> checkIfPrerequisite(int numCourses, vector<vector<int>>& prerequisites,
                                   vector<vector<int>>& queries) {
    std::vector<Node> courses(numCourses);
    for (const auto pIndexes : prerequisites) {
      courses[pIndexes[1]].addPrerequisite(&courses[pIndexes[0]]);
    }
    std::vector<bool> result;
    result.reserve(queries.size());
    for (const auto query : queries) {
      result.emplace_back(courses[query[1]].isPrerequisite(&courses[query[0]]));
    }
    return result;
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
