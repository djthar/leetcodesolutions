#include <gtest/gtest.h>

#include "solution.h"

class SUITE_NAME :
    public ::testing::TestWithParam<
        std::tuple<std::tuple<int, vector<vector<int>>, vector<vector<int>>>, vector<bool>>> {
protected:
  void SetUp() override {
    auto input    = std::get<0>(GetParam());
    numCourses    = std::get<0>(input);
    prerequisites = std::get<1>(input);
    queries       = std::get<2>(input);
    expected      = std::get<1>(GetParam());
  }

  Solution            solution;
  int                 numCourses    = {};
  vector<vector<int>> prerequisites = {};
  vector<vector<int>> queries       = {};
  vector<bool>        result        = {};
  vector<bool>        expected      = {};
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.checkIfPrerequisite(numCourses, prerequisites, queries);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(
    Tests, SUITE_NAME,
    ::testing::Values(std::make_tuple(std::make_tuple(3,
                                                      vector<vector<int>>{
                                                        {1, 2},
                                                        {1, 0},
                                                        {2, 0}
},
                                                      vector<vector<int>>{{1, 0}, {1, 2}}),

                                      vector<bool>{true, true}),

                      std::make_tuple(std::make_tuple(2, vector<vector<int>>{{1, 0}},
                                                      vector<vector<int>>{{0, 1}, {1, 0}}),

                                      vector<bool>{false, true}),

                      std::make_tuple(std::make_tuple(2, vector<vector<int>>{},
                                                      vector<vector<int>>{{0, 1}, {1, 0}}),

                                      vector<bool>{false, false})));
