#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <algorithm>
#include <array>
#include <memory>
#include <string>

using namespace std;

class Node {
public:
  void addWord(string_view word) {
    if (word.empty()) {
      isLeaf_ = true;
      return;
    }
    const auto nextIndex = word[0] - 'a';
    if (childs_[nextIndex] == nullptr) { childs_[nextIndex] = make_unique<Node>(); }
    childs_[nextIndex]->addWord(word.substr(1));
  }

  [[nodiscard]] bool search(string_view word) const {
    if (word.empty()) { return isLeaf_; }
    const auto nextWord = word.substr(1);
    if (word[0] == '.') {
      return std::any_of(childs_.begin(), childs_.end(), [nextWord](const unique_ptr<Node>& node) {
        return node != nullptr && node->search(nextWord);
      });
    }
    const auto  nextIndex = word[0] - 'a';
    const auto& node      = childs_[nextIndex];
    if (node == nullptr) { return false; }
    return node->search(nextWord);
  }

private:
  std::array<unique_ptr<Node>, 'z' - 'a' + 1> childs_;
  bool                                        isLeaf_ = false;
};

class WordDictionary {
public:
  WordDictionary() = default;

  void        addWord(const string& word) { root_.addWord(word); }

  bool        search(const string& word) { return root_.search(word); }

  std::string toString() { return ""; }

private:
  Node root_;
};

/**
 * Your WordDictionary object will be instantiated and called as such:
 * WordDictionary* obj = new WordDictionary();
 * obj->addWord(word);
 * bool param_2 = obj->search(word);
 */

#endif  // LEETCODEEXERCISES_SOLUTION_H
