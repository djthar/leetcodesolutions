#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <array>
#include <string>
#include <utility>

using namespace std;

class BrowserHistory {
public:
  explicit BrowserHistory(string homepage) { history_[actual_] = std::move(homepage); }

  void visit(string url) {
    advanceActual();
    if (actual_ == first_) { advanceFirst(); }
    history_[actual_] = std::move(url);
    last_             = actual_;
  }

  string back(int steps) {
    steps = std::min(steps, static_cast<int>(getBackHistorySize()));
    if (actual_ >= steps) {
      actual_ -= steps;
    } else {
      actual_ = historyMaxSize_ - (steps - actual_);
    }
    return history_[actual_];
  }

  string forward(int steps) {
    steps = std::min(steps, static_cast<int>(getForwardHistorySize()));
    if (actual_ + steps < historyMaxSize_) {
      actual_ += steps;
    } else {
      actual_ = actual_ + steps - historyMaxSize_;
    }
    return history_[actual_];
  }

  [[nodiscard]] std::string toString() const {
    std::string result;
    size_t      actual = first_;
    size_t      last   = getNextIndex(last_);
    while (actual != last) {
      if (actual == actual_) { result += "["; }
      result += history_[actual];
      if (actual == actual_) { result += "]"; }
      result += "\n";
      ++actual;
      if (actual == historyMaxSize_) { actual = 0; }
    }
    return result;
  }

private:
  void          advanceFirst() { first_ = getNextIndex(first_); }

  void          advanceActual() { actual_ = getNextIndex(actual_); }

  static size_t getNextIndex(size_t index) {
    ++index;
    if (index >= historyMaxSize_) { index = 0; }
    return index;
  }

  [[nodiscard]] size_t getBackHistorySize() const {
    if (actual_ >= first_) { return actual_ - first_; }
    return historyMaxSize_ - first_ + actual_;
  }

  [[nodiscard]] size_t getForwardHistorySize() const {
    if (last_ >= actual_) { return last_ - actual_; }
    return historyMaxSize_ - actual_ + last_;
  }

  static const size_t                      historyMaxSize_ = 100;
  std::array<std::string, historyMaxSize_> history_;
  size_t                                   first_  = 0;
  size_t                                   last_   = 0;
  size_t                                   actual_ = 0;
};

/**
 * Your BrowserHistory object will be instantiated and called as such:
 * BrowserHistory* obj = new BrowserHistory(homepage);
 * obj->visit(url);
 * string param_2 = obj->back(steps);
 * string param_3 = obj->forward(steps);
 */

#endif  // LEETCODEEXERCISES_SOLUTION_H
