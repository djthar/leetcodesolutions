#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <cstdlib>
#include <iostream>
#include <queue>
#include <vector>
using namespace std;

enum class Color {
  UNVISITED,
  RED,
  BLACK
};

class Solution {
  int                 n_;
  vector<vector<int>> neighbours_;
  vector<vector<int>> components_;
  vector<Color>       colors_;

  void                initNeighboursAndColors(int n, vector<vector<int>>& edges) {
    n_ = n;
    neighbours_.resize(n);
    colors_ = vector<Color>(n, Color::UNVISITED);
    for (const auto& edge : edges) {
      const auto n1 = edge[0] - 1;
      const auto n2 = edge[1] - 1;
      neighbours_[n1].emplace_back(n2);
      neighbours_[n2].emplace_back(n1);
    }
  }

  std::string_view toString(Color color) {
    switch (color) {
      case Color::UNVISITED :
        return "unvisited";
      case Color::RED :
        return "red";
      case Color::BLACK :
        return "black";
      default :

        break;
    }
    return "error";
  }

  bool isBipartite(int startNode, Color color, vector<int>& component) {
    // std::cout << __FUNCTION__ << " " << startNode + 1 << " " << toString(color) << " vs "
    //           << toString(colors_[startNode]) << std::endl;
    if (colors_[startNode] != Color::UNVISITED) { return color == colors_[startNode]; }
    colors_[startNode] = color;
    component.emplace_back(startNode);
    for (auto neighbour : neighbours_[startNode]) {
      if (!isBipartite(neighbour, color == Color::RED ? Color::BLACK : Color::RED, component)) {
        // std::cout << "Not bipartite on neighbour " << neighbour + 1 << std::endl;
        return false;
      }
    }

    return true;
  }

  int getMaxGroupsInComponent(const vector<int>& component) {
    int maxDepth = component.empty() ? 0 : 1;
    for (auto node : component) {
      vector<int> nodesDepths(n_, -1);
      queue<int>  bfsQueue;
      bfsQueue.push(node);
      nodesDepths[node] = 1;
      while (not bfsQueue.empty()) {
        int visitedNode = bfsQueue.front();
        bfsQueue.pop();
        for (auto neighbour : neighbours_[visitedNode]) {
          if (nodesDepths[neighbour] != -1) { continue; }
          nodesDepths[neighbour] = nodesDepths[visitedNode] + 1;
          maxDepth               = max(maxDepth, nodesDepths[neighbour]);
          bfsQueue.push(neighbour);
        }
      }
    }
    return maxDepth;
  }

public:
  int magnificentSets(int n, vector<vector<int>>& edges) {
    initNeighboursAndColors(n, edges);
    components_.emplace_back();
    for (int i = 0; i < n_; ++i) {
      // std::cout << "start bipartite search on " << i + 1 << std::endl;
      if (colors_[i] == Color::UNVISITED and not isBipartite(i, Color::RED, components_.back())) {
        return -1;
      }
      if (not components_.back().empty()) { components_.emplace_back(); }
    }
    // std::cout << components_.size() << std::endl;
    int total = 0;
    for (const auto& component : components_) { total += getMaxGroupsInComponent(component); }
    return total;
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
