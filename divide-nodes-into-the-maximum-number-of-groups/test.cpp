#include <gtest/gtest.h>

#include "solution.h"

class SUITE_NAME :
    public ::testing::TestWithParam<std::tuple<std::tuple<int, vector<vector<int>>>, int>> {
protected:
  void SetUp() override {
    auto input = std::get<0>(GetParam());
    n          = std::get<0>(input);
    edges      = std::get<1>(input);
    expected   = std::get<1>(GetParam());
  }

  Solution            solution;
  int                 n        = {};
  vector<vector<int>> edges    = {};
  int                 result   = {};
  int                 expected = {};
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.magnificentSets(n, edges);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(
    Tests, SUITE_NAME,
    ::testing::Values(
        std::make_tuple(std::make_tuple(6,
                                        vector<vector<int>>{
                                          {1, 2},
                                          {1, 4},
                                          {1, 5},
                                          {2, 6},
                                          {2, 3},
                                          {4, 6}
}),
                        4),
        std::make_tuple(std::make_tuple(3, vector<vector<int>>{{1, 2}, {2, 3}, {3, 1}}),

                        -1),
        std::make_tuple(
            std::make_tuple(92,
                            vector<vector<int>>{
                              {67, 29}, {13, 29}, {77, 29}, {36, 29}, {82, 29}, {54, 29}, {57, 29},
                              {53, 29}, {68, 29}, {26, 29}, {21, 29}, {46, 29}, {41, 29}, {45, 29},
                              {56, 29}, {88, 29}, {2, 29},  {7, 29},  {5, 29},  {16, 29}, {37, 29},
                              {50, 29}, {79, 29}, {91, 29}, {48, 29}, {87, 29}, {25, 29}, {80, 29},
                              {71, 29}, {9, 29},  {78, 29}, {33, 29}, {4, 29},  {44, 29}, {72, 29},
                              {65, 29}, {61, 29}}),

            57)));
