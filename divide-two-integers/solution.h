#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <cstdlib>
#include <limits>
#include <optional>

using namespace std;

class Solution {
  optional<int> getEarlyReturn(int dividend, int divisor) {
    if (divisor == dividend) { return 1; }
    const bool isPositive = (dividend >= 0 and divisor >= 0) or (dividend < 0 and divisor < 0);
    if ((dividend >= 0 and divisor >= 0 and divisor > dividend)
        or (dividend < 0 and divisor < 0 and divisor < dividend)) {
      return 0;
    }
    if (1 == divisor) {
      return dividend;
    } else if (-1 == divisor) {
      if (dividend == numeric_limits<int>::min()) { return numeric_limits<int>::max(); }
      return -dividend;
    } else if (numeric_limits<int>::min() == divisor or numeric_limits<int>::max() == divisor) {
      return dividend == numeric_limits<int>::min() ? (isPositive ? 1 : -1) : 0;
    }
    if (2 == divisor and dividend > 0) { return dividend >> 1; }
    return std::nullopt;
  }

  int _divide(int dividend, int divisor) {
    // cout << dividend << "/" << divisor << endl;
    auto earlyReturn = getEarlyReturn(dividend, divisor);
    if (earlyReturn) { return *earlyReturn; }
    int  accDivisor = divisor;
    int  result     = 1;
    bool isBreak    = false;
    while ((dividend - accDivisor) > 0) {
      const int newResult     = result << 1;
      const int newAccDivisor = accDivisor << 1;
      if (newResult < 0 or newAccDivisor < 0) {
        // cout << "break\n";
        isBreak = true;
        break;
      }
      result     = newResult;
      accDivisor = newAccDivisor;
    }
    if (!isBreak) {
      result     >>= 1;
      accDivisor >>= 1;
    }

    // cout << "r=" << result << " acc=" << accDivisor << endl;
    if (result > 1) { result += _divide(dividend - accDivisor, divisor); }
    return result;
  }

public:
  int divide(int dividend, int divisor) {
    // cout << "-----\n" << dividend << "/" << divisor << "\n";
    auto earlyReturn = getEarlyReturn(dividend, divisor);
    if (earlyReturn) { return *earlyReturn; }
    const bool isPositive = (dividend >= 0 and divisor >= 0) or (dividend < 0 and divisor < 0);
    int        result     = 0;
    if (dividend == numeric_limits<int>::min()) {
      result = ((divisor & 1) == 1) ? _divide(abs(dividend + 1), abs(divisor))
                                    : _divide(abs(dividend) >> 1, abs(divisor) >> 1);
    } else {
      result = _divide(abs(dividend), abs(divisor));
    }

    return isPositive ? result : -result;
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
