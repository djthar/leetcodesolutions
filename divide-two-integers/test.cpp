#include <gtest/gtest.h>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<int, int, int>> {
protected:
  void SetUp() override {
    dividend = std::get<0>(GetParam());
    divisor  = std::get<1>(GetParam());
    expected = std::get<2>(GetParam());
  }

  Solution solution;
  int      dividend = 0;
  int      divisor  = 0;
  int      expected = 0;
  int      result   = 0;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.divide(dividend, divisor);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(Tests, SUITE_NAME,
                        ::testing::Values(std::make_tuple(10, 3, 3), std::make_tuple(7, -3, -2),
                                          std::make_tuple(0, 1, 0), std::make_tuple(1, 1, 1)));
