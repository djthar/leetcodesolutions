#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <algorithm>
#include <vector>

using namespace std;

class Solution {
public:
  vector<int> searchRange(vector<int>& nums, int target) {
    const auto startPoint = lower_bound(nums.begin(), nums.end(), target);
    if (startPoint == nums.end()) { return {-1, -1}; }
    if (*startPoint != target) { return {-1, -1}; }
    const auto endPoint = upper_bound(nums.begin(), nums.end(), target) - 1;
    return {static_cast<int>(startPoint - nums.begin()), static_cast<int>(endPoint - nums.begin())};
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
