#include <gtest/gtest.h>

#include <unordered_set>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<vector<int>, int, vector<int>>> {
protected:
  void SetUp() override {
    input    = std::get<0>(GetParam());
    target   = std::get<1>(GetParam());
    expected = std::get<2>(GetParam());
  }

  Solution    solution;
  vector<int> input;
  int         target = 0;
  vector<int> expected;
  vector<int> result;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.searchRange(input, target);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(
    Tests, SUITE_NAME,
    ::testing::Values(std::make_tuple(vector<int>({5, 7, 7, 8, 8, 10}), 8, vector<int>({3, 4})),
                      std::make_tuple(vector<int>({5, 7, 7, 8, 8, 10}), 6, vector<int>({-1, -1})),
                      std::make_tuple(vector<int>({}), 0, vector<int>({-1, -1}))));
