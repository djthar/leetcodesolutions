#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <algorithm>
#include <vector>

using namespace std;

class Solution {
public:
  int findDuplicate(vector<int>& nums) {
    sort(nums.begin(), nums.end());
    for (size_t i = 0; i < nums.size() - 1; ++i) {
      if (nums[i] == nums[i + 1]) { return nums[i]; }
    }
    return 0;
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
