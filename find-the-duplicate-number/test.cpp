#include <gtest/gtest.h>

#include <unordered_set>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<vector<int>, int>> {
protected:
  void SetUp() override {
    input    = std::get<0>(GetParam());
    expected = std::get<1>(GetParam());
  }

  Solution    solution;
  vector<int> input;
  int         expected = 0;
  int         result   = 0;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.findDuplicate(input);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(Tests, SUITE_NAME,
                        ::testing::Values(std::make_tuple(vector<int>({1, 3, 4, 2, 2}), 2),
                                          std::make_tuple(vector<int>({3, 1, 3, 4, 2}), 3),
                                          std::make_tuple(vector<int>({1, 1}), 1),
                                          std::make_tuple(vector<int>({1, 1, 2}), 1)));
