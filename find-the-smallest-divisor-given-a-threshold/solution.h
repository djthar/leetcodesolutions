#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <algorithm>
#include <limits>
#include <vector>

using namespace std;

class Solution {
  using divisorRange_t = tuple<int, int>;

public:
  int smallestDivisor(vector<int>& nums, int threshold) {
    if (nums.size() == threshold) { return *(max_element(nums.begin(), nums.end())); }
    divisorRange_t searchRange = {0, *(max_element(nums.begin(), nums.end()))};
    while (get<1>(searchRange) != get<0>(searchRange) + 1) {
      const int m   = getMiddlePointInRange(searchRange);
      const int sum = getDivisorSum(nums, m);
      if (sum <= threshold) {
        get<1>(searchRange) = m;
      } else {
        get<0>(searchRange) = m;
      }
    }
    return get<1>(searchRange);
  }

private:
  static int getDivisorSum(const vector<int>& nums, int divisor) {
    if (0 == divisor) { return numeric_limits<int>::max(); }
    int result = 0;
    for (int n : nums) { result += (n / divisor) + (n % divisor == 0 ? 0 : 1); }
    return result;
  }

  static int getMiddlePointInRange(const divisorRange_t& r) { return (get<0>(r) + get<1>(r)) / 2; }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
