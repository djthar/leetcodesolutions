#include <gtest/gtest.h>

#include <unordered_set>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<vector<int>, int, int>> {
protected:
  void SetUp() override {
    input     = std::get<0>(GetParam());
    threshold = std::get<1>(GetParam());
    expected  = std::get<2>(GetParam());
  }

  Solution    solution;
  vector<int> input;
  int         threshold = 0;
  int         expected  = 0;
  int         result    = 0;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.smallestDivisor(input, threshold);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(Tests, SUITE_NAME,
                        ::testing::Values(std::make_tuple(vector<int>({1, 2, 5, 9}), 6, 5),
                                          std::make_tuple(vector<int>({44, 22, 33, 11, 1}), 5, 44),
                                          std::make_tuple(vector<int>({21212, 10101, 12121}),
                                                          1000000, 1),
                                          std::make_tuple(vector<int>({2, 3, 5, 7, 11}), 11, 3)));
