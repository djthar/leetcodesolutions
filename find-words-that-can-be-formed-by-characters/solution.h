#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

class Solution {
public:
  int countCharacters(vector<string>& words, string chars) {
    initCharsMap(chars);
    int result = 0;
    for (const auto& word : words) {
      if (word.size() > chars.size()) { continue; }
      result += canBeFormed(word) ? word.size() : 0;
    }
    return result;
  }

protected:
  bool canBeFormed(const string& word) {
    std::unordered_map<char, int> charsCount;
    for (char letter : word) {
      if (const auto& charsCountIt = charsCount_.find(letter); charsCountIt == charsCount_.end()) {
        return false;
      } else {
        auto [value, inserted] = charsCount.try_emplace(letter, 0);
        ++value->second;
        if (value->second > charsCountIt->second) { return false; }
      }
    }
    return true;
  }

  void initCharsMap(const string& word) {
    for (char letter : word) {
      auto [value, inserted] = charsCount_.try_emplace(letter, 0);
      ++value->second;
    }
  }

  std::unordered_map<char, int> charsCount_;
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
