#include <gtest/gtest.h>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<vector<string>, string, int>> {
protected:
  static string vectorToString(const vector<int>& v) {
    string result;
    for (auto value : v) { result += to_string(value) + ", "; }
    if (!result.empty()) {
      result.pop_back();
      result.pop_back();
    }
    return result;
  }

  void SetUp() override {
    words    = std::get<0>(GetParam());
    chars    = std::get<1>(GetParam());
    expected = std::get<2>(GetParam());
  }

  Solution       solution;
  vector<string> words;
  string         chars;
  int            result;
  int            expected;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.countCharacters(words, chars);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(
    Tests, SUITE_NAME,
    ::testing::Values(std::make_tuple(vector<string>({"cat", "bt", "hat", "tree"}), "atach", 6),
                      std::make_tuple(vector<string>({"hello", "world", "leetcode"}),
                                      "welldonehoneyr", 10)));
