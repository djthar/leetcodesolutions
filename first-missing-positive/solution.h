#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <algorithm>
#include <vector>

using namespace std;

class Solution {
public:
  int firstMissingPositive(vector<int>& nums) {
    sort(nums.begin(), nums.end());
    auto firstPositive = upper_bound(nums.begin(), nums.end(), 0);
    if (firstPositive == nums.end() or *firstPositive != 1) { return 1; }
    for (auto it = firstPositive; it != nums.end() - 1; ++it) {
      if (*(it + 1) > (*it) + 1) { return (*it) + 1; }
    }
    return nums.back() + 1;
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
