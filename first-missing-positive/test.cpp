#include <gtest/gtest.h>

#include <unordered_set>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<vector<int>, int>> {
protected:
  void SetUp() override {
    input    = std::get<0>(GetParam());
    expected = std::get<1>(GetParam());
  }

  Solution    solution;
  vector<int> input;
  int         expected = 0;
  int         result   = 0;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.firstMissingPositive(input);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(Tests, SUITE_NAME,
                        ::testing::Values(std::make_tuple(vector<int>({1, 2, 0}), 3),
                                          std::make_tuple(vector<int>({3, 4, -1, 1}), 2),
                                          std::make_tuple(vector<int>({7, 8, 9, 11, 12}), 1),
                                          std::make_tuple(vector<int>({0, 2, 2, 1, 1}), 3)));
