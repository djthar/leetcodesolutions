#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <functional>
#include <semaphore>

using namespace std;

class FizzBuzz {
private:
  int                   n;
  int                   count = 0;
  std::binary_semaphore fizzSmph{0};
  std::binary_semaphore buzzSmph{0};
  std::binary_semaphore fizzbuzzSmph{0};
  std::binary_semaphore numberSmph{0};

public:
  FizzBuzz(int n) {
    this->n = n;
    increaseCountAndRelease();
  }

  // printFizz() outputs "fizz".
  void fizz(function<void()> printFizz) {
    while (true) {
      fizzSmph.acquire();
      if (count > n) { return; }
      printFizz();
      increaseCountAndRelease();
    }
  }

  // printBuzz() outputs "buzz".
  void buzz(function<void()> printBuzz) {
    while (true) {
      buzzSmph.acquire();
      if (count > n) { return; }
      printBuzz();
      increaseCountAndRelease();
    }
  }

  // printFizzBuzz() outputs "fizzbuzz".
  void fizzbuzz(function<void()> printFizzBuzz) {
    while (true) {
      fizzbuzzSmph.acquire();
      if (count > n) { return; }
      printFizzBuzz();
      increaseCountAndRelease();
    }
  }

  // printNumber(x) outputs "x", where x is an integer.
  void number(function<void(int)> printNumber) {
    while (true) {
      numberSmph.acquire();
      if (count > n) { return; }
      printNumber(count);
      increaseCountAndRelease();
    }
  }

private:
  void increaseCountAndRelease() {
    ++count;
    if (count > n) {
      fizzbuzzSmph.release();
      fizzSmph.release();
      buzzSmph.release();
      numberSmph.release();
    } else {
      getNextSmph().release();
    }
  }

  std::binary_semaphore& getNextSmph() {
    if (count % 3 == 0 and count % 5 == 0) {
      return fizzbuzzSmph;
    } else if (count % 3 == 0) {
      return fizzSmph;
    } else if (count % 5 == 0) {
      return buzzSmph;
    } else {
      return numberSmph;
    }
  }
};

using Solution = FizzBuzz;

#endif  // LEETCODEEXERCISES_SOLUTION_H
