#include <gtest/gtest.h>

#include <future>
#include <unordered_set>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<int, std::vector<string>>> {
protected:
  void SetUp() override {
    input    = std::get<0>(GetParam());
    expected = std::get<1>(GetParam());
    solution = std::make_unique<Solution>(input);
  }

  std::unique_ptr<Solution> solution;
  int                       input;
  vector<string>            expected;
  vector<string>            result;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result.reserve(input);
  const auto        printNumber   = [this](int number) { result.push_back(to_string(number)); };
  const auto        printFizz     = [this]() { result.push_back("fizz"); };
  const auto        printBuzz     = [this]() { result.push_back("buzz"); };
  const auto        printFizzBuzz = [this]() { result.push_back("fizzbuzz"); };
  std::future<void> thread1
      = std::async(std::launch::async, [this, &printNumber]() { solution->number(printNumber); });
  std::future<void> thread2
      = std::async(std::launch::async, [this, &printFizz]() { solution->fizz(printFizz); });
  std::future<void> thread3 = std::async(
      std::launch::async, [this, &printFizzBuzz]() { solution->fizzbuzz(printFizzBuzz); });
  std::future<void> thread4
      = std::async(std::launch::async, [this, &printBuzz]() { solution->buzz(printBuzz); });
  thread1.wait();
  thread2.wait();
  thread3.wait();
  thread4.wait();
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(Tests, SUITE_NAME,
                        ::testing::Values(std::make_tuple(
                            15, std::vector<string>({"1", "2", "fizz", "4", "buzz", "fizz", "7",
                                                     "8", "fizz", "buzz", "11", "fizz", "13", "14",
                                                     "fizzbuzz"}))));
