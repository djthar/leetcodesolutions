get_filename_component(problem_name ${CMAKE_CURRENT_SOURCE_DIR} NAME)
string(REPLACE " " "_" problem_name ${problem_name})
string(REPLACE "-" "_" problem_name ${problem_name})
add_executable(${problem_name} test.cpp)
target_link_libraries(${problem_name} PRIVATE gtest_main)
target_compile_definitions(${problem_name} PRIVATE SUITE_NAME=${problem_name}_Ts)