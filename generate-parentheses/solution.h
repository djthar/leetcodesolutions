#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <queue>
#include <string>
#include <vector>

using namespace std;

class Solution {
public:
  using ParenthesisNode = tuple<string, int, int>;

  vector<string> generateParenthesis(int n) {
    nNodes_ = n;
    queue_.push({"(", 1, 0});
    while (!queue_.empty()) {
      iterateOnParenthesisNodeQueueFront();
      queue_.pop();
    }
    return std::move(res);
  }

private:
  void iterateOnParenthesisNodeQueueFront() {
    ParenthesisNode node   = queue_.front();
    const int       closed = get<2>(node);
    if (closed == nNodes_) {
      res.push_back(std::move(get<0>(node)));
      return;
    }
    const int open = get<1>(node);
    if (open < nNodes_) { openNewBracketAndAddToQueue(node); }
    if (closed < open) { closeOneBracketAndAddToQueue(std::move(node)); }
  }

  void openNewBracketAndAddToQueue(const ParenthesisNode& node) {
    ParenthesisNode next  = node;
    get<0>(next)         += '(';
    ++get<1>(next);
    queue_.push(std::move(next));
  }

  void closeOneBracketAndAddToQueue(ParenthesisNode&& node) {
    get<0>(node) += ')';
    ++get<2>(node);
    queue_.push(std::move(node));
  }

  int                    nNodes_ = 1;
  queue<ParenthesisNode> queue_;
  vector<string>         res;
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
