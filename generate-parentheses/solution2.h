#ifndef LEETCODEEXERCISES_SOLUTION2_H
#define LEETCODEEXERCISES_SOLUTION2_H

#include <queue>
#include <sstream>
#include <stdexcept>
#include <string>
#include <unordered_set>
#include <vector>

using namespace std;

class Solution2 {
  class Node {
  public:
    Node(bool isRoot) : isRoot_(isRoot) {}

    Node() = default;

    size_t size() const {
      // cout << __func__ << " isRoot_=" << isRoot_ <<  endl;
      size_t s = isRoot_ ? 0 : 1;
      for (const auto& n : nodes_) { s += n.size(); }
      return s;
    }

    void   addNode() { nodes_.emplace_back(); }

    string toString() const {
      stringstream ss;
      if (not isRoot_) { ss << "("; }
      for (const auto& n : nodes_) { ss << n.toString(); }
      if (not isRoot_) { ss << ")"; }
      return ss.str();
    }

    Node& operator[](size_t i) {
      // cout << __func__ << " " << i << endl;
      if (0 == i) { return *this; }
      --i;
      size_t vectorIndex = 0;
      while (i >= nodes_[vectorIndex].size() && vectorIndex < nodes_.size()) {
        // cout << __func__ << " size=" << nodes_.size() << " vectorIndex=" << vectorIndex << " i="
        // << i << endl;
        i -= nodes_[vectorIndex].size();
        ++vectorIndex;
      }
      if (vectorIndex < nodes_.size()) { return nodes_[vectorIndex][i]; }
      throw std::out_of_range("Node access out of range");
    }

    vector<Node> nodes_;
    bool         isRoot_ = false;
  };

public:
  vector<string> generateParenthesis(int n) {
    queue<Node> nodes;
    nodes.push(Node(true));
    nodes.front().addNode();
    unordered_set<string> visitedNodes;
    visitedNodes.emplace(nodes.front().toString());
    int lastN = 0;
    while (size_t actualNodeSize = nodes.front().size() < n) {
      // if (actualNodeSize != lastN) { visitedNodes.clear(); }
      auto node = nodes.front();
      // cout << "actual node size=" << nodes.front().size() << endl;
      // cout << "actual queue size=" << nodes.size() << endl;
      for (size_t i = 0; i <= node.size(); ++i) {
        Node newNode = node;
        // cout << "adding node at index=" << i << " to node with size=" << newNode.size() << endl;
        newNode[i].addNode();
        // cout << newNode.size() << endl;
        auto [it, inserted] = visitedNodes.emplace(newNode.toString());
        if (inserted) { nodes.push(std::move(newNode)); }
      }
      nodes.pop();
    }

    vector<string> result;
    while (not nodes.empty()) {
      result.emplace_back(nodes.front().toString());
      nodes.pop();
    }
    return result;
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION2_H
