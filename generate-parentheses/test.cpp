#include <gtest/gtest.h>

#include "solution.h"
#include "solution2.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<int, vector<string>>> {
protected:
  void SetUp() override {
    input    = std::get<0>(GetParam());
    expected = std::get<1>(GetParam());
  }

  Solution       solution;
  int            input = 0;
  vector<string> expected;
  vector<string> result;
};

class generate_parentheses_Ts_2 : public ::testing::TestWithParam<std::tuple<int, vector<string>>> {
protected:
  void SetUp() override {
    input    = std::get<0>(GetParam());
    expected = std::get<1>(GetParam());
  }

  Solution2      solution;
  int            input = 0;
  vector<string> expected;
  vector<string> result;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.generateParenthesis(input);
  EXPECT_EQ(result, expected);
}

TEST_P(generate_parentheses_Ts_2, CheckIfSolutionCorrect) {
  result = solution.generateParenthesis(input);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(Tests, SUITE_NAME,
                        ::testing::Values(std::make_tuple(3, vector<string>({"((()))", "(()())",
                                                                             "(())()", "()(())",
                                                                             "()()()"})),
                                          std::make_tuple(1, vector<string>({"()"}))));

INSTANTIATE_TEST_SUITE_P(Tests, generate_parentheses_Ts_2,
                        ::testing::Values(std::make_tuple(3, vector<string>({"()()()", "(())()",
                                                                             "()(())", "(()())",
                                                                             "((()))"})),
                                          std::make_tuple(1, vector<string>({"()"}))));
