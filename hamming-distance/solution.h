#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <bitset>
#include <type_traits>

using namespace std;

#define IS_INTEGRAL(T) typename std::enable_if<std::is_integral<T>::value>::type* = 0

template<class T>
auto toBitset(T byte, IS_INTEGRAL(T)) -> std::bitset<sizeof(T) * 8> {
  return byte;
}

class Solution {
public:
  int hammingDistance(int x, int y) {
    return static_cast<int>((toBitset(x) ^ toBitset(y)).count());
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
