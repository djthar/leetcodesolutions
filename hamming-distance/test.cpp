#include <gtest/gtest.h>

#include <unordered_set>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<int, int, int>> {
protected:
  void SetUp() override {
    x        = std::get<0>(GetParam());
    y        = std::get<1>(GetParam());
    expected = std::get<2>(GetParam());
  }

  Solution solution;
  int      x        = 0;
  int      y        = 0;
  int      expected = 0;
  int      result   = 0;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.hammingDistance(x, y);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(Tests, SUITE_NAME,
                        ::testing::Values(std::make_tuple(1, 4, 2), std::make_tuple(3, 1, 1),
                                          std::make_tuple(93, 73, 2)));
