#include <gtest/gtest.h>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<int, string>> {
protected:
  void SetUp() override {
    input    = std::get<0>(GetParam());
    expected = std::get<1>(GetParam());
  }

  Solution solution;
  int      input = 0;
  string   expected;
  string   result;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.intToRoman(input);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(Tests, SUITE_NAME,
                        ::testing::Values(std::make_tuple(3, "III"), std::make_tuple(4, "IV"),
                                          std::make_tuple(9, "IX"), std::make_tuple(58, "LVIII"),
                                          std::make_tuple(1994, "MCMXCIV")));
