#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <deque>
#include <vector>

using namespace std;

struct JumpData {
  JumpData(size_t n, size_t e) : nJumps(n), endIndex(e) {}

  size_t nJumps;
  size_t endIndex;
};

class Solution {
public:
  int jump(vector<int>& nums) {
    if (nums.size() == 1) { return 0; }
    deque<JumpData> jumpData;
    jumpData.emplace_back(1, nums[0]);

    for (size_t i = 1; i < (nums.size() - 1); ++i) {
      const auto& bestJumpData = jumpData.front();
      jumpData.emplace_back(bestJumpData.nJumps + 1, i + nums[i]);
      while (jumpData.front().endIndex <= i) { jumpData.pop_front(); }
    }
    return static_cast<int>(jumpData.front().nJumps);
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
