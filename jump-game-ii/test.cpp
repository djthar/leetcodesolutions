#include <gtest/gtest.h>

#include <unordered_set>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<std::vector<int>, int>> {
protected:
  void SetUp() override {
    n        = std::get<0>(GetParam());
    expected = std::get<1>(GetParam());
  }

  Solution         solution;
  std::vector<int> n;
  int              expected;
  int              result;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.jump(n);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(Tests, SUITE_NAME,
                        ::testing::Values(std::make_tuple(std::vector{2, 3, 1, 1, 4}, 2),
                                          std::make_tuple(std::vector{2, 3, 0, 1, 4}, 2),
                                          std::make_tuple(std::vector{3, 4, 3, 2, 5, 4, 3}, 3),
                                          std::make_tuple(std::vector{1, 2}, 1),
                                          std::make_tuple(std::vector{0}, 0)));
