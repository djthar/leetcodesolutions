#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <cmath>
#include <string>
#include <string_view>
#include <unordered_map>
#include <vector>

using namespace std;

class Solution {
public:
  vector<string> letterCombinations(string_view digits) {
    if (digits.empty()) { return {}; }
    vector<string> result;
    result.reserve(static_cast<size_t>(pow(4, digits.size())));
    const auto subLetterCombinations = letterCombinations(digits.substr(1));
    for (const auto& letter : keyboard_.at(digits[0])) {
      if (subLetterCombinations.empty()) {
        result.emplace_back(1, letter);
      } else {
        for (const auto& subLetterCombination : subLetterCombinations) {
          result.emplace_back(letter + subLetterCombination);
        }
      }
    }
    return result;
  }

  const std::unordered_map<char, vector<char>> keyboard_ = {
    {{'2', {'a', 'b', 'c'}},
     {'3', {'d', 'e', 'f'}},
     {'4', {'g', 'h', 'i'}},
     {'5', {'j', 'k', 'l'}},
     {'6', {'m', 'n', 'o'}},
     {'7', {'p', 'q', 'r', 's'}},
     {'8', {'t', 'u', 'v'}},
     {'9', {'w', 'x', 'y', 'z'}}}
  };
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
