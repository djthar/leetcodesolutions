#include <gtest/gtest.h>

#include <unordered_set>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<string, vector<string>>> {
protected:
  void SetUp() override {
    input    = std::get<0>(GetParam());
    expected = std::get<1>(GetParam());
  }

  Solution       solution;
  string         input;
  vector<string> expected;
  vector<string> result;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.letterCombinations(input);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(Tests, SUITE_NAME,
                        ::testing::Values(std::make_tuple("23", vector<string>({"ad", "ae", "af",
                                                                                "bd", "be", "bf",
                                                                                "cd", "ce", "cf"})),
                                          std::make_tuple("", vector<string>()),
                                          std::make_tuple("2", vector<string>({"a", "b", "c"}))));
