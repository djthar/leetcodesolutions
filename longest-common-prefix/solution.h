#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <algorithm>
#include <string>
#include <vector>

using namespace std;

class Solution {
public:
  string longestCommonPrefix(vector<string>& strs) {
    size_t maxLen = 200;
    for (const auto& s : strs) { maxLen = min(maxLen, s.size()); }
    for (size_t i = 0; i < maxLen; ++i) {
      const char c    = strs[0][i];
      bool       isOk = true;
      for (size_t j = 1; j < strs.size(); ++j) {
        if (c != strs[j][i]) {
          isOk = false;
          break;
        }
      }
      if (not isOk) { return strs[0].substr(0, i); }
    }
    return strs[0].substr(0, maxLen);
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
