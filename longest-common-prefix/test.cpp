#include <gtest/gtest.h>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<vector<string>, string>> {
protected:
  void SetUp() override {
    input    = std::get<0>(GetParam());
    expected = std::get<1>(GetParam());
  }

  Solution       solution;
  vector<string> input;
  string         expected;
  string         result;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.longestCommonPrefix(input);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(
    Tests, SUITE_NAME,
    ::testing::Values(std::make_tuple(vector<string>({"flower", "flow", "flight"}), "fl"),
                      std::make_tuple(vector<string>({"dog", "racecar", "car"}), "")));
