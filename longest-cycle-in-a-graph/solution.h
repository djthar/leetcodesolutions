#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <vector>

using namespace std;

class Node {
public:
  Node* child     = nullptr;
  bool  visited   = false;
  int   depth     = 0;
  int   iteration = 0;
};

class Solution {
public:
  int longestCycle(vector<int>& edges) {
    nodes_.resize(edges.size());
    for (size_t i = 0; i < edges.size(); ++i) {
      if (edges[i] >= 0) { nodes_[i].child = &nodes_[edges[i]]; }
    }
    int iteration = 0;
    for (auto& node : nodes_) {
      if (!node.visited) {
        ++iteration;
        visit(node, 0, iteration);
      }
    }

    return maxCycleLength;
  }

private:
  void visit(Node& node, int depth, int iteration) {
    node.visited   = true;
    node.depth     = depth;
    node.iteration = iteration;
    Node* child    = node.child;
    if (child == nullptr) { return; }
    if (!child->visited) {
      visit(*child, depth + 1, iteration);
    } else if (child->iteration == iteration) {
      const int cycleLength = depth + 1 - child->depth;
      maxCycleLength        = max(cycleLength, maxCycleLength);
    }
  }

  std::vector<Node> nodes_;
  int               maxCycleLength = -1;
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
