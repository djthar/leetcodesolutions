#include <gtest/gtest.h>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<vector<int>, int>> {
protected:
  static string vectorToString(const vector<int>& v) {
    string result;
    for (auto value : v) { result += to_string(value) + ", "; }
    if (!result.empty()) {
      result.pop_back();
      result.pop_back();
    }
    return result;
  }

  void SetUp() override {
    input    = std::get<0>(GetParam());
    expected = std::get<1>(GetParam());
  }

  Solution    solution;
  vector<int> input;
  int         result;
  int         expected;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.longestCycle(input);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(
    Tests, SUITE_NAME,
    ::testing::Values(std::make_tuple(vector<int>({3, 3, 4, 2, 3}), 3),
                      std::make_tuple(vector<int>({3, 4, 0, 2, -1, 2}), 3),
                      std::make_tuple(vector<int>({1, 2, 0, 4, 5, 6, 3, 8, 9, 7}), 4),
                      std::make_tuple(vector<int>({-1, 4, -1, 2, 0, 4}), -1),
                      std::make_tuple(vector<int>({2, -1, 3, 1}), -1)));
