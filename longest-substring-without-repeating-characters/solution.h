#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <array>
#include <string>

using namespace std;

class Solution {
public:
  int lengthOfLongestSubstring(string s) {
    int                  startIndex = 0;
    int                  endIndex   = 0;
    std::array<int, 127> currentChars{};
    currentChars.fill(-1);
    int maxLen = 0;
    while (endIndex < s.size()) {
      auto currentCharIndex = currentChars[s[endIndex]];
      if (currentCharIndex >= startIndex && currentCharIndex < endIndex) {
        maxLen     = max(maxLen, endIndex - startIndex);
        startIndex = currentCharIndex + 1;
      }
      currentChars[s[endIndex]] = endIndex;
      ++endIndex;
    }
    return max(maxLen, endIndex - startIndex);
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
