#include <gtest/gtest.h>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<std::string, int>> {
protected:
  void SetUp() override {
    input    = std::get<0>(GetParam());
    expected = std::get<1>(GetParam());
  }

  Solution    solution;
  std::string input;
  int         result   = 0;
  int         expected = 0;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.lengthOfLongestSubstring(input);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(Tests, SUITE_NAME,
                        ::testing::Values(std::make_tuple("abcabcbb", 3),
                                          std::make_tuple("bbbbb", 1), std::make_tuple("pwwkew", 3),
                                          std::make_tuple("", 0)));
