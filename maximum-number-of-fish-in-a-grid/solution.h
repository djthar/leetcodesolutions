#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <cstdlib>
#include <vector>
using namespace std;

class Solution {
public:
  int findMaxFish(vector<vector<int>>& grid) {
    grid_  = &grid;
    height = grid.size();
    width  = height != 0 ? grid[0].size() : 0;
    // std::cout << "size=" << width << "," << height << std::endl;
    visited.reserve(grid.size());
    for (size_t i = 0; i < height; ++i) { visited.emplace_back(vector<bool>(width, false)); }

    for (size_t i = 0; i < height; ++i) {
      for (size_t j = 0; j < width; ++j) {
        // std::cout << "findMaxFish " << i << "," << j << " visited=" << visited[i][j] << ",
        // fishes=" << grid[i][j] << std::endl;
        if (visited[i][j] or grid[i][j] == 0) { continue; }
        int fishes = findMaxFishFrom(i, j);
        if (fishes > result) { result = fishes; }
      }
    }
    return result;
  }

  int findMaxFishFrom(int i, int j) {
    // std::cout << "findMaxFishFrom " << i << "," << j << " visited=" << visited[i][j] << ",
    // fishes=" << (*grid_)[i][j] << std::endl;
    if (visited[i][j]) { return 0; }
    // std::cout << "visit " << i << "," << j << std::endl;
    visited[i][j] = true;
    int fishes    = (*grid_)[i][j];
    for (auto neighbour : getAvailableNeighbours(i, j)) {
      fishes += findMaxFishFrom(neighbour.first, neighbour.second);
    }
    return fishes;
  }

  std::vector<pair<int, int>> getAvailableNeighbours(int i, int j) {
    std::vector<pair<int, int>> availableNeighbours;
    availableNeighbours.reserve(4);
    if (i > 0 and !visited[i - 1][j] != 0 and (*grid_)[i - 1][j] != 0) {
      // std::cout << "Add down" << std::endl;
      availableNeighbours.emplace_back(i - 1, j);
    }
    if (i < height - 1 and !visited[i + 1][j] != 0 and (*grid_)[i + 1][j] != 0) {
      availableNeighbours.emplace_back(i + 1, j);
      // std::cout << "Add up" << std::endl;
    }
    if (j > 0 and !visited[i][j - 1] != 0 and (*grid_)[i][j - 1] != 0) {
      availableNeighbours.emplace_back(i, j - 1);
      // std::cout << "Add left" << std::endl;
    }
    if (j < width - 1 and !visited[i][j + 1] != 0 and (*grid_)[i][j + 1] != 0) {
      availableNeighbours.emplace_back(i, j + 1);
      // std::cout << "Add right" << std::endl;
    }
    return availableNeighbours;
  }

  int                  width  = 0;
  int                  height = 0;
  int                  result = 0;
  vector<vector<bool>> visited;
  vector<vector<int>>* grid_;
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
