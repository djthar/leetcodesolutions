#include <gtest/gtest.h>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<vector<vector<int>>, int>> {
protected:
  void SetUp() override {
    input    = std::get<0>(GetParam());
    expected = std::get<1>(GetParam());
  }

  Solution            solution;
  vector<vector<int>> input    = {};
  int                 result   = {};
  int                 expected = {};
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.findMaxFish(input);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(
    Tests, SUITE_NAME,
    ::testing::Values(std::make_tuple(
                          vector<vector<int>>{
                            {0, 2, 1, 0},
                            {4, 0, 0, 3},
                            {1, 0, 0, 4},
                            {0, 3, 2, 0}
},
                          7),
                      std::make_tuple(vector<vector<int>>{{1, 0, 0, 0},
                                                          {
                                                            0,
                                                            0,
                                                            0,
                                                          },
                                                          {0, 0, 0, 0},
                                                          {
                                                            0,
                                                            0,
                                                            0,
                                                          }},
                                      1)));
