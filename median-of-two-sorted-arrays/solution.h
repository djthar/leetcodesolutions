#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <algorithm>
#include <vector>

using namespace std;

class Solution {
public:
  double findMedianSortedArrays(vector<int>& nums1, vector<int>& nums2) {
    nums1.reserve(nums1.size() + nums2.size());
    for (auto num : nums2) { insert_sorted(nums1, num); }
    const size_t upIndex = nums1.size() / 2;
    if (0 == nums1.size() % 2) {
      return static_cast<double>(nums1[upIndex - 1] + nums1[upIndex]) / 2.0;
    } else {
      return static_cast<double>(nums1[upIndex]);
    }
  }

  template<typename T>
  typename std::vector<T>::iterator insert_sorted(std::vector<T>& vec, T const& item) {
    return vec.insert(std::upper_bound(vec.begin(), vec.end(), item), item);
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
