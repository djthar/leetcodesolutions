#include <gtest/gtest.h>

#include "solution.h"

class SUITE_NAME :
    public ::testing::TestWithParam<std::tuple<std::vector<int>, std::vector<int>, double>> {
protected:
  void SetUp() override {
    input_1  = std::get<0>(GetParam());
    input_2  = std::get<1>(GetParam());
    expected = std::get<2>(GetParam());
  }

  Solution         solution;
  std::vector<int> input_1;
  std::vector<int> input_2;
  double           expected = 0;
  double           result   = 0;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.findMedianSortedArrays(input_1, input_2);
  EXPECT_DOUBLE_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(
    Tests, SUITE_NAME,
    ::testing::Values(std::make_tuple(std::vector<int>({1, 3}), std::vector<int>({2}), 2),
                      std::make_tuple(std::vector<int>({1, 2}), std::vector<int>({3, 4}), 2.5),
                      std::make_tuple(std::vector<int>({0, 0}), std::vector<int>({0, 0}), 0),
                      std::make_tuple(std::vector<int>({}), std::vector<int>({1}), 1),
                      std::make_tuple(std::vector<int>({2}), std::vector<int>({}), 2)));
