#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <map>
#include <vector>
using namespace std;

struct ListNode {
  int       val;
  ListNode* next;

  ListNode() : val(0), next(nullptr) {}

  ListNode(int x) : val(x), next(nullptr) {}

  ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:
  ListNode* mergeKLists(vector<ListNode*>& lists) {
    map<int, tuple<ListNode*, ListNode*>> memory;
    for (ListNode* node : lists) {
      while (nullptr != node) {
        auto [nodeIt, inserted] = memory.emplace(node->val, tuple(node, node));
        if (not inserted) {
          ListNode*& lastNode = std::get<1>(nodeIt->second);
          lastNode->next      = node;
          lastNode            = node;
        }
        node                              = node->next;
        std::get<1>(nodeIt->second)->next = nullptr;
      }
    }
    auto memoryLeft = memory.begin();
    if (memoryLeft == memory.end()) { return nullptr; }
    auto memoryRight = memory.begin();
    ++memoryRight;
    while (memoryRight != memory.end()) {
      get<1>(memoryLeft->second)->next = get<0>(memoryRight->second);
      ++memoryLeft;
      ++memoryRight;
    }
    return get<0>(memory.begin()->second);
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
