#include <gtest/gtest.h>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<vector<vector<int>>, vector<int>>> {
protected:
  static ListNode* createList(const vector<int>& v) {
    if (v.empty()) { return nullptr; }
    auto*     front      = new ListNode(v.front());
    ListNode* actualNode = front;
    for (auto it = v.begin() + 1; it != v.end(); ++it) {
      actualNode->next = new ListNode(*it);
      actualNode       = actualNode->next;
    }
    return front;
  }

  static void freeList(ListNode* l) {
    if (nullptr == l) { return; }
    ListNode* next = l->next;
    delete l;
    freeList(next);
  }

  static vector<int> createVector(ListNode* l) {
    vector<int> v;
    while (nullptr != l) {
      v.emplace_back(l->val);
      l = l->next;
    }
    return v;
  }

  static string vectorToString(const vector<int>& v) {
    string result;
    for (auto value : v) { result += to_string(value) + ", "; }
    if (!result.empty()) {
      result.pop_back();
      result.pop_back();
    }
    return result;
  }

  void SetUp() override {
    const auto& testInput = std::get<0>(GetParam());
    for (const auto& l : testInput) { input.emplace_back(createList(l)); }
    expected = std::get<1>(GetParam());
  }

  void              TearDown() override { freeList(result); }

  Solution          solution;
  vector<ListNode*> input;
  ListNode*         result = nullptr;
  vector<int>       expected;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result       = solution.mergeKLists(input);
  auto resultV = createVector(result);
  ASSERT_EQ(resultV.size(), expected.size())
      << "result and expected are of unequal length and have contents " << vectorToString(resultV)
      << " vs " << vectorToString(expected);
  for (int i = 0; i < resultV.size(); ++i) {
    EXPECT_EQ(resultV[i], expected[i]) << "result and expected differ at index " << i;
  }
}

INSTANTIATE_TEST_SUITE_P(Tests, SUITE_NAME,
                        ::testing::Values(std::make_tuple(vector<vector<int>>({
                                                            {1, 4, 5},
                                                            {1, 3, 4},
                                                            {2, 6}
}),
                                                          vector<int>({1, 1, 2, 3, 4, 4, 5, 6})),
                                          std::make_tuple(vector<vector<int>>(), vector<int>()),
                                          std::make_tuple(vector<vector<int>>(1), vector<int>())));
