#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

using namespace std;

struct ListNode {
  int       val;
  ListNode* next;

  ListNode() : val(0), next(nullptr) {}

  ListNode(int x) : val(x), next(nullptr) {}

  ListNode(int x, ListNode* next) : val(x), next(next) {}
};

// NOTE: Accepts a recursive solution
class Solution {
public:
  ListNode* mergeTwoLists(ListNode* list1, ListNode* list2) {
    ListNode* mergedList = nullptr;
    ListNode* actual     = mergedList;
    bool      stop       = false;
    do {
      ListNode* nextNode;
      if (list1 == nullptr) {
        nextNode = list2;
        stop     = true;
      } else if (list2 == nullptr) {
        nextNode = list1;
        stop     = true;
      } else if (list1->val < list2->val) {
        nextNode = list1;
        list1    = list1->next;
      } else {
        nextNode = list2;
        list2    = list2->next;
      }
      if (nullptr == mergedList) {
        mergedList = nextNode;
        actual     = mergedList;
      } else {
        actual->next = nextNode;
        actual       = actual->next;
      }
    } while (not stop);
    return mergedList;
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
