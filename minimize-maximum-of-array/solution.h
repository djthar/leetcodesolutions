#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <algorithm>
#include <vector>

using namespace std;

class Solution {
public:
  int minimizeArrayValue(vector<int>& nums) {
    if (nums.size() == 1) { return nums[1]; }
    unsigned long prefixSum = 0;
    int           maxValue  = 0;
    for (unsigned int i = 0; i < nums.size(); ++i) {
      prefixSum += nums[i];
      maxValue   = max(maxValue, getMeanValue(prefixSum, i + 1));
    }
    return maxValue;
  }

  static inline int getMeanValue(unsigned long sum, unsigned int nElements) {
    return static_cast<int>((sum + nElements - 1) / nElements);
  }
};

class Solution2 {
public:
  int minimizeArrayValue(vector<int>& nums) {
    if (nums.size() == 1) { return nums[1]; }
    int maxValue   = *max_element(nums.begin(), nums.end());
    int lowerValue = 0;

    while (lowerValue < maxValue) {
      int midValue = lowerValue + (maxValue - lowerValue) / 2;
      if (canLowerArrayToValue(nums, midValue)) {
        maxValue = midValue;
      } else {
        lowerValue = midValue + 1;
      }
    }
    return maxValue;
  }

  static bool canLowerArrayToValue(const vector<int>& nums, int value) {
    unsigned long movements = 0;
    for (auto num : nums) {
      if (num <= value) {
        movements += value - num;
      } else {
        if (movements < (num - value)) { return false; }
        movements -= num - value;
      }
    }
    return true;
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
