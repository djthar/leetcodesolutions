#include <gtest/gtest.h>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<vector<int>, int>> {
protected:
  static string vectorToString(const vector<int>& v) {
    string result;
    for (auto value : v) { result += to_string(value) + ", "; }
    if (!result.empty()) {
      result.pop_back();
      result.pop_back();
    }
    return result;
  }

  void SetUp() override {
    input    = std::get<0>(GetParam());
    expected = std::get<1>(GetParam());
  }

  Solution    solution;
  vector<int> input;
  int         result;
  int         expected;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.minimizeArrayValue(input);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(Tests, SUITE_NAME,
                        ::testing::Values(std::make_tuple(vector<int>({3, 7, 1, 6}), 5),
                                          std::make_tuple(vector<int>({0, 7, 1, 6}), 4),
                                          std::make_tuple(vector<int>({10, 1}), 10),
                                          std::make_tuple(vector<int>({13, 13, 20, 0, 8, 9, 9}),
                                                          16),
                                          std::make_tuple(vector<int>({6, 9, 3, 8, 14}), 8)));
