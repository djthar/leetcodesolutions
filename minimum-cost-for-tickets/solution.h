#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <algorithm>
#include <unordered_map>
#include <vector>

using namespace std;

class Solution {
public:
  int mincostTickets(vector<int>& days, vector<int>& costs) {
    costs[0] = min(costs[0], min(costs[1], costs[2]));
    costs[1] = min(costs[1], costs[2]);
    return getMinCostForDay(0, days, costs);
  }

  int getMinCostForDay(size_t index, vector<int>& days, vector<int>& cost) {
    if (index >= days.size()) { return 0; }
    const auto existingValue = memonize.find(index);
    if (existingValue != memonize.end()) { return existingValue->second; }
    int        minCost1day = cost[0] + getMinCostForDay(index + 1, days, cost);
    const auto nextIndex7day
        = std::find_if(days.begin() + index, days.end(),
                       [index, &days](int day) { return day >= days[index] + 7; });
    if (nextIndex7day != days.end()) {
      const size_t ni7            = nextIndex7day - days.begin();
      int          minCost7days   = min(cost[1] + getMinCostForDay(ni7, days, cost), minCost1day);
      const auto   nextIndex30day = std::find_if(
          nextIndex7day, days.end(), [index, &days](int day) { return day >= days[index] + 30; });
      if (nextIndex30day != days.end()) {
        const size_t ni30       = nextIndex30day - days.begin();
        const int minCost30days = min(cost[2] + getMinCostForDay(ni30, days, cost), minCost7days);
        memonize.emplace(index, minCost30days);
        return minCost30days;
      }
      minCost7days = min(minCost7days, cost[2]);
      memonize.emplace(index, minCost7days);
      return minCost7days;
    }
    minCost1day = min(minCost1day, cost[1]);
    memonize.emplace(index, minCost1day);
    return minCost1day;
  }

private:
  unordered_map<int, int> memonize;
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
