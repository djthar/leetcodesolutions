#include <gtest/gtest.h>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<vector<int>, vector<int>, int>> {
protected:
  static string vectorToString(const vector<int>& v) {
    string result;
    for (auto value : v) { result += to_string(value) + ", "; }
    if (!result.empty()) {
      result.pop_back();
      result.pop_back();
    }
    return result;
  }

  void SetUp() override {
    days     = std::get<0>(GetParam());
    costs    = std::get<1>(GetParam());
    expected = std::get<2>(GetParam());
  }

  Solution    solution;
  vector<int> days;
  vector<int> costs;
  int         result;
  int         expected;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.mincostTickets(days, costs);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(
    Tests, SUITE_NAME,
    ::testing::Values(std::make_tuple(vector<int>({1, 4, 6, 7, 8, 20}), vector<int>({2, 7, 15}),
                                      11),
                      std::make_tuple(vector<int>({1, 4, 6, 7, 8, 20}), vector<int>({7, 2, 15}), 6),
                      std::make_tuple(vector<int>({1, 2, 3, 4, 6, 8, 9, 10, 13, 14, 16, 17, 19, 21,
                                                   24, 26, 27, 28, 29}),
                                      vector<int>({3, 14, 50}), 50),
                      std::make_tuple(vector<int>({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 30, 31}),
                                      vector<int>({2, 7, 15}), 17)));
