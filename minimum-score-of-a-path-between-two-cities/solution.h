#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <deque>
#include <limits>
#include <memory>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

class Node {
public:
  void addNeighbour(int node, int weight) {
    auto insertion = neighbours_.emplace(node, weight);
    if (!insertion.second) { insertion.first->second = min(insertion.first->second, weight); }
  }

  const auto& getNeighbours() { return neighbours_; }

private:
  unordered_map<int, int> neighbours_;
};

class Solution {
public:
  int minScore(int n, vector<vector<int>>& roads) {
    for (const auto& road : roads) {
      const auto node1insertion = nodes_.emplace(road[0], Node());
      const auto node2insertion = nodes_.emplace(road[1], Node());
      node1insertion.first->second.addNeighbour(road[1], road[2]);
      node2insertion.first->second.addNeighbour(road[0], road[2]);
    }
    std::unordered_set<int> visited;
    int                     minScore = numeric_limits<int>::max();
    deque<int>              nodes;
    nodes.push_back(1);
    while (!nodes.empty()) {
      int node = nodes.front();
      nodes.pop_front();
      visited.emplace(node);
      const auto& neighbours = nodes_.at(node).getNeighbours();
      for (const auto& neigbour : neighbours) {
        if (visited.count(neigbour.first) != 0) { continue; }
        minScore = min(neigbour.second, minScore);
        nodes.push_back(neigbour.first);
      }
    }
    return minScore;
  }

private:
  std::unordered_map<int, Node> nodes_;
};

using LinkedSpace = std::tuple<int, std::unordered_set<size_t>>;

class Solution2 {
public:
  int minScore(int n, vector<vector<int>>& roads) {
    for (const auto& road : roads) {
      auto space1 = linkedSpaces_.emplace(
          road[0], make_shared<LinkedSpace>(
                       make_tuple(road[2], unordered_set<size_t>({static_cast<size_t>(road[0])}))));
      auto space2 = linkedSpaces_.emplace(
          road[1], make_shared<LinkedSpace>(
                       make_tuple(road[2], unordered_set<size_t>({static_cast<size_t>(road[1])}))));
      auto& linkedSpace1    = space1.first->second;
      get<0>(*linkedSpace1) = min(road[2], get<0>(*linkedSpace1));
      auto& linkedSpace2    = space2.first->second;
      get<0>(*linkedSpace2) = min(road[2], get<0>(*linkedSpace2));
      if (linkedSpace1.get() == linkedSpace2.get()) { continue; }

      auto& destiny = get<1>(*linkedSpace1).size() > get<1>(*linkedSpace2).size() ? linkedSpace1
                                                                                  : linkedSpace2;
      auto  origin  = destiny.get() == linkedSpace1.get() ? linkedSpace2 : linkedSpace1;

      if (get<0>(*destiny) < get<0>(*origin)) {
        get<0>(*origin) = get<0>(*destiny);
      } else {
        get<0>(*destiny) = get<0>(*origin);
      }

      get<1>(*destiny).insert(get<1>(*origin).begin(), get<1>(*origin).end());
      for (auto node : get<1>(*origin)) { linkedSpaces_.at(node) = destiny; }
    }
    return get<0>(*linkedSpaces_.at(n));
  }

private:
  std::unordered_map<size_t, std::shared_ptr<LinkedSpace>> linkedSpaces_;
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
