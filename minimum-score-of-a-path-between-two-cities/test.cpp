#include <gtest/gtest.h>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<int, vector<vector<int>>, int>> {
protected:
  static string vectorToString(const vector<int>& v) {
    string result;
    for (auto value : v) { result += to_string(value) + ", "; }
    if (!result.empty()) {
      result.pop_back();
      result.pop_back();
    }
    return result;
  }

  void SetUp() override {
    target   = std::get<0>(GetParam());
    input    = std::get<1>(GetParam());
    expected = std::get<2>(GetParam());
  }

  Solution            solution;
  vector<vector<int>> input;
  int                 target = 0;
  int                 result;
  int                 expected;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.minScore(target, input);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(
    Tests, SUITE_NAME,
    ::testing::Values(std::make_tuple(4,
                                      vector<vector<int>>({
                                        {1, 2, 9},
                                        {2, 3, 6},
                                        {2, 4, 5},
                                        {1, 4, 7}
}),
                                      5),
                      std::make_tuple(6,
                                      vector<vector<int>>({{4, 5, 7468},
                                                           {6, 2, 7173},
                                                           {6, 3, 8365},
                                                           {2, 3, 7674},
                                                           {5, 6, 7852},
                                                           {1, 2, 8547},
                                                           {2, 4, 1885},
                                                           {2, 5, 5192},
                                                           {1, 3, 4065},
                                                           {1, 4, 7357}}),
                                      1885),
                      std::make_tuple(20,
                                      vector<vector<int>>({{18, 20, 9207},
                                                           {14, 12, 1024},
                                                           {11, 9, 3056},
                                                           {8, 19, 416},
                                                           {3, 18, 5898},
                                                           {17, 3, 6779},
                                                           {13, 15, 3539},
                                                           {15, 11, 1451},
                                                           {19, 2, 3805},
                                                           {9, 8, 2238},
                                                           {1, 16, 618},
                                                           {16, 14, 55},
                                                           {17, 7, 6903},
                                                           {12, 13, 1559},
                                                           {2, 17, 3693}}),
                                      55),
                      std::make_tuple(4, vector<vector<int>>({{1, 2, 2}, {1, 3, 4}, {3, 4, 7}}),
                                      2)));
