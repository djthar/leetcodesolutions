#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Solution {
public:
  vector<string> createChessboard() {
    return vector<string>(chessboardSize_, string(chessboardSize_, '.'));
  }

  vector<size_t> createIndicesRow() {
    vector<size_t> row;
    row.reserve(chessboardSize_);
    for (size_t i = 0; i < chessboardSize_; ++i) { row.emplace_back(i); }
    return row;
  }

  vector<vector<size_t>> createAvailableIndices() {
    vector<vector<size_t>> indices;
    indices.reserve(chessboardSize_);
    for (size_t i = 0; i < chessboardSize_; ++i) { indices.emplace_back(createIndicesRow()); }
    return indices;
  }

  vector<vector<size_t>> replaceRowByOneIndexRow(vector<vector<size_t>>&& indexes, int row,
                                                 int col) {
    indexes[row] = vector<size_t>(1, col);
    return std::move(indexes);
  }

  void printAvailableIndices(const vector<vector<size_t>>& availableIndices) {
    cout << "---available indices--\n";
    for (const auto& r : availableIndices) {
      for (auto i : r) { cout << i << " "; }
      cout << "\n";
    }
    cout << "-----\n";
  }

  void removeQueenIndices(vector<vector<size_t>> availableIndices, size_t col, size_t row) {
    availableIndices = replaceRowByOneIndexRow(std::move(availableIndices), row, col);
    // cout << __func__ << " size=" << chessboardSize_ << " row=" << row << " col=" << col << endl;
    if (row == (chessboardSize_ - 1) and not availableIndices[row].empty()) {
      // printAvailableIndices(availableIndices);
      // cout << "This seems ok\n";
      vector<string> chessboard = createChessboard();
      for (size_t i = 0; i < chessboardSize_; ++i) {
        chessboard[i][availableIndices[i].front()] = 'Q';
      }
      result.emplace_back(std::move(chessboard));
      return;
    }

    for (size_t r = row + 1; r < chessboardSize_; ++r) {
      auto&        rowIndices           = availableIndices[r];
      const size_t rowDistance          = r - row;
      const bool   leftIndexInRange     = col >= rowDistance;
      const bool   rightIndexInRange    = (col + rowDistance) < chessboardSize_;
      auto         isQueenAtackPosition = [=](size_t c) {
        const bool shouldRemove = c == col or (leftIndexInRange and c == (col - rowDistance))
                               or (rightIndexInRange and c == (col + rowDistance));
        return shouldRemove;
      };
      for (auto it = std::find_if(rowIndices.begin(), rowIndices.end(), isQueenAtackPosition);
           it != rowIndices.end();
           it = std::find_if(rowIndices.begin(), rowIndices.end(), isQueenAtackPosition)) {
        rowIndices.erase(it);
      }
      if (rowIndices.empty()) {
        // cout << "some row is empty\n";
        return;
      }
    }

    auto& nextRow = availableIndices[row + 1];
    for (auto nextRowAvailableCol : nextRow) {
      removeQueenIndices(availableIndices, nextRowAvailableCol, row + 1);
    }
    // cout << "There is no possibilities in this starting col\n";
  }

  vector<vector<string>> solveNQueens(int n) {
    if (2 == n or 3 == n) { return {}; }
    chessboardSize_ = n;
    result.clear();

    for (size_t col = 0; col < chessboardSize_; ++col) {
      removeQueenIndices(createAvailableIndices(), col, 0);
      /*
      if (availableIndices) {
          printAvailableIndices(*availableIndices);
      }*/
    }
    return result;
  }

  size_t                 chessboardSize_ = 0;
  vector<vector<string>> result;
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
