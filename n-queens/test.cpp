#include <gtest/gtest.h>

#include <unordered_set>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<int, vector<vector<string>>>> {
protected:
  void SetUp() override {
    input    = std::get<0>(GetParam());
    expected = std::get<1>(GetParam());
  }

  Solution               solution;
  int                    input = 0;
  vector<vector<string>> expected;
  vector<vector<string>> result;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.solveNQueens(input);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(Tests, SUITE_NAME,
                        ::testing::Values(std::make_tuple(4, vector<vector<string>>({
                                                               {".Q..", "...Q", "Q...", "..Q."},
                                                               {"..Q.", "Q...", "...Q", ".Q.."}
})),
                                          std::make_tuple(1, vector<vector<string>>({{"Q"}}))));
