#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <deque>
#include <unordered_set>
#include <vector>

using namespace std;

class Node {
public:
  std::unordered_set<int> childs;
  bool                    visited = false;
};

class Solution {
public:
  int makeConnected(int n, vector<vector<int>>& connections) {
    if (connections.size() < n - 1) { return -1; }
    nodes_.resize(n);
    for (const auto& connection : connections) {
      auto& node1 = nodes_[connection[0]];
      node1.childs.emplace(connection[1]);
      auto& node2 = nodes_[connection[1]];
      node2.childs.emplace(connection[0]);
    }

    int unconnectedGroups = 0;
    for (auto& node : nodes_) {
      if (!node.visited) {
        ++unconnectedGroups;
        visitBfs(node);
      }
    }
    return unconnectedGroups - 1;
  }

private:
  void visit(Node& node) {
    node.visited = true;
    for (int childIdx : node.childs) {
      Node& childNode = nodes_[childIdx];
      if (!childNode.visited) { visit(childNode); }
    }
  }

  void visitBfs(Node& node) {
    node.visited = true;
    deque<Node*> nodes;
    nodes.emplace_back(&node);
    while (!nodes.empty()) {
      auto pNode = nodes.front();
      nodes.pop_front();
      for (auto childIdx : pNode->childs) {
        Node& childNode = nodes_[childIdx];
        if (!childNode.visited) {
          nodes.push_back(&childNode);
          childNode.visited = true;
        }
      }
    }
  }

  std::vector<Node> nodes_;
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
