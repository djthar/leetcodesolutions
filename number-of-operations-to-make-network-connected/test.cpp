#include <gtest/gtest.h>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<int, vector<vector<int>>, int>> {
protected:
  static string vectorToString(const vector<int>& v) {
    string result;
    for (auto value : v) { result += to_string(value) + ", "; }
    if (!result.empty()) {
      result.pop_back();
      result.pop_back();
    }
    return result;
  }

  void SetUp() override {
    target   = std::get<0>(GetParam());
    input    = std::get<1>(GetParam());
    expected = std::get<2>(GetParam());
  }

  Solution            solution;
  vector<vector<int>> input;
  int                 target = 0;
  int                 result;
  int                 expected;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.makeConnected(target, input);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(
    Tests, SUITE_NAME,
    ::testing::Values(
        std::make_tuple(4,
                        vector<vector<int>>({
                          {0, 1},
                          {0, 2},
                          {1, 2}
}),
                        1),
        std::make_tuple(6, vector<vector<int>>({{0, 1}, {0, 2}, {0, 3}, {1, 2}, {1, 3}}), 2),
        std::make_tuple(6, vector<vector<int>>({{0, 1}, {0, 2}, {0, 3}, {1, 2}}), -1)));
