#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <algorithm>
#include <unordered_set>
#include <vector>

using namespace std;

class Solution {
public:
  using Key = tuple<vector<int>, vector<int>>;

  struct KeyHasher {
    std::size_t operator()(const Key& vec) const {
      std::size_t seed = get<0>(vec).size() + get<1>(vec).size();
      for (auto& i : get<0>(vec)) { seed ^= i + 0x9e3779b9 + (seed << 6) + (seed >> 2); }
      for (auto& i : get<1>(vec)) { seed ^= i + 0x9e3779b9 + (seed << 6) + (seed >> 2); }
      return seed;
    }
  };

  using UniqueResultContainer = unordered_set<Key, KeyHasher>;

  void createSquares() {
    for (int i = 0; i <= 9; ++i) { squares_.emplace(i * i); }
  }

  inline bool areAllItemsEqualAndFulfillsSquareCondition(const vector<int>& nums) {
    return std::all_of(nums.begin() + 1, nums.end(), [&](int r) { return r == nums.front(); })
       and 1 == squares_.count(nums.back() + nums.front());
  }

  inline bool areAllItemsEqualButNotFulfillsSquareCondition(const vector<int>& nums) {
    return std::all_of(nums.begin() + 1, nums.end(), [&](int r) { return r == nums.front(); })
       and 0 == squares_.count(nums.back() + nums.front());
  }

  inline bool doAdditionToBackFulfillsSquareCondition(const vector<int>& nums, int number) {
    return nums.empty() or 1 == squares_.count(number + nums.back());
  }

  void permuteUniqueRecursive(vector<int>&& nums, vector<int>&& prefix = {}) {
    auto [it, inserted] = result.emplace(nums, prefix);
    if (not inserted) { return; }
    if (nums.size() == 1 or areAllItemsEqualAndFulfillsSquareCondition(nums)) {
      if (prefix.empty() or 1 == squares_.count(prefix.back() + nums.front())) { ++resultCount_; }
      return;
    }
    if (areAllItemsEqualButNotFulfillsSquareCondition(nums)) { return; }
    for (size_t i = 0; i < nums.size(); ++i) {
      const int actualNum = nums[i];
      if (not doAdditionToBackFulfillsSquareCondition(prefix, actualNum)) { continue; }
      vector<int> numsCopyWithoutActualNum;
      numsCopyWithoutActualNum.reserve(nums.size() + 1);
      for (size_t j = 0; j < nums.size(); ++j) {
        if (j != i) { numsCopyWithoutActualNum.emplace_back(nums[j]); }
      }
      vector<int> nextPrefix;
      nextPrefix.reserve(prefix.size() + 1);
      nextPrefix = prefix;
      nextPrefix.emplace_back(actualNum);
      permuteUniqueRecursive(std::move(numsCopyWithoutActualNum), std::move(nextPrefix));
    }
  }

  int numSquarefulPerms(vector<int>& nums) {
    createSquares();
    permutationsSize_ = nums.size();
    permuteUniqueRecursive(std::move(nums));
    result.clear();
    return resultCount_;
  }

  size_t                permutationsSize_;
  int                   resultCount_ = 0;
  UniqueResultContainer result;
  unordered_set<int>    squares_;
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
