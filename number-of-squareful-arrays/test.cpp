#include <gtest/gtest.h>

#include "solution.h"
#include "solution2.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<vector<int>, int>> {
protected:
  void SetUp() override {
    input    = std::get<0>(GetParam());
    expected = std::get<1>(GetParam());
  }

  Solution    solution;
  vector<int> input;
  int         expected;
  int         result;
};

class number_of_squareful_arrays_Ts_2 :
    public ::testing::TestWithParam<std::tuple<vector<int>, int>> {
protected:
  void SetUp() override {
    input    = std::get<0>(GetParam());
    expected = std::get<1>(GetParam());
  }

  Solution2   solution;
  vector<int> input;
  int         expected;
  int         result;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.numSquarefulPerms(input);
  EXPECT_EQ(result, expected);
}

TEST_P(number_of_squareful_arrays_Ts_2, CheckIfSolutionCorrect) {
  result = solution.numSquarefulPerms(input);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(Tests, SUITE_NAME,
                        ::testing::Values(std::make_tuple(vector<int>({1, 17, 8}), 2),
                                          std::make_tuple(vector<int>({2, 2, 2}), 1)));

INSTANTIATE_TEST_SUITE_P(Tests, number_of_squareful_arrays_Ts_2,
                        ::testing::Values(std::make_tuple(vector<int>({1, 17, 8}), 2),
                                          std::make_tuple(vector<int>({2, 2, 2}), 1)));
