#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <string>
#include <vector>

using namespace std;

const int MOD = 1e9 + 7;

class Solution {
public:
  int ways(vector<string>& pizza, int k) {
    initSize(pizza);
    initPresum(pizza);
    initDp(k);
    return remainingWaysInRectangle(k - 1, 0, 0);
  }

  int remainingWaysInRectangle(int remainingCuts, int rowCut, int colCut) {
    if (presum[rowCut][colCut] == 0) {
      return 0;  // There are no more apples left
    }
    if (remainingCuts == 0) {
      return 1;  // We are at the last cut
    }
    if (dp[remainingCuts][rowCut][colCut] != -1) {
      return dp[remainingCuts][rowCut][colCut];  // We've already computed this case
    }
    int result = 0;

    for (int nextRowCut = rowCut + 1; nextRowCut < pizzaRows; nextRowCut++) {
      if (getApplesInRectangle(rowCut, colCut, nextRowCut, colCut) > 0) {
        result = (result + remainingWaysInRectangle(remainingCuts - 1, nextRowCut, colCut)) % MOD;
      }
    }
    for (int nextColCut = colCut + 1; nextColCut < pizzaCols; nextColCut++) {
      if (getApplesInRectangle(rowCut, colCut, rowCut, nextColCut) > 0) {
        result = (result + remainingWaysInRectangle(remainingCuts - 1, rowCut, nextColCut)) % MOD;
      }
    }
    dp[remainingCuts][rowCut][colCut] = result;
    return result;
  }

  [[nodiscard]] int getApplesInRectangle(int row, int col, int rowEnd, int colEnd) const {
    return presum[row][col] - presum[rowEnd][colEnd];
  }

  void initSize(const vector<string>& pizza) {
    pizzaRows = static_cast<int>(pizza.size());
    pizzaCols = static_cast<int>(pizza[0].size());
  }

  void initPresum(const vector<string>& pizza) {
    presum = vector<vector<int>>(pizzaRows + 1, vector<int>(pizzaCols + 1, 0));
    for (int i = pizzaRows - 1; i >= 0; i--) {
      for (int j = pizzaCols - 1; j >= 0; j--) {
        presum[i][j] = presum[i][j + 1] + presum[i + 1][j] - presum[i + 1][j + 1]
                     + (pizza[i][j] == 'A' ? 1 : 0);
      }
    }
  }

  void initDp(int nCuts) {
    dp = vector<vector<vector<int>>>(vector(nCuts, vector(pizzaRows, vector(pizzaCols, -1))));
  }

private:
  int                         pizzaRows = 0;
  int                         pizzaCols = 0;
  vector<vector<int>>         presum;
  vector<vector<vector<int>>> dp;
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
