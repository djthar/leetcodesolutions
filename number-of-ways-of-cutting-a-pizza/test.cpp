#include <gtest/gtest.h>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<vector<string>, int, int>> {
protected:
  static string vectorToString(const vector<int>& v) {
    string result;
    for (auto value : v) { result += to_string(value) + ", "; }
    if (!result.empty()) {
      result.pop_back();
      result.pop_back();
    }
    return result;
  }

  void SetUp() override {
    pizza    = std::get<0>(GetParam());
    k        = std::get<1>(GetParam());
    expected = std::get<2>(GetParam());
  }

  Solution       solution;
  vector<string> pizza;
  int            k;
  int            result;
  int            expected;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.ways(pizza, k);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(
    Tests, SUITE_NAME,
    ::testing::Values(std::make_tuple(vector<string>({"A..", "AAA", "..."}), 3, 3),
                      std::make_tuple(vector<string>({"A..", "AA.", "..."}), 3, 1),
                      std::make_tuple(vector<string>({"A..", "A..", "..."}), 1, 1)));
