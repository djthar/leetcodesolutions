#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <cstdint>
#include <string>
#include <unordered_set>

using namespace std;

class Solution {
public:
  int partitionString(string s) {
    int      nSubStrings = 1;
    uint32_t flag        = 0;
    for (char letter : s) {
      uint32_t val = 1 << (letter - 'a');
      if (flag & val) {
        flag = val;
        ++nSubStrings;
      }
      flag |= val;
    }
    return nSubStrings;
  }
};

class Solution2 {
public:
  int partitionString(string s) {
    int                 nSubStrings = 1;
    unordered_set<char> foundLetters;
    for (char letter : s) {
      auto insertionResult = foundLetters.emplace(letter);
      if (!insertionResult.second) {
        ++nSubStrings;
        foundLetters.clear();
        foundLetters.emplace(letter);
      }
    }
    return nSubStrings;
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
