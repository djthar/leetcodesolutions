#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <cstdint>
#include <string>
#include <vector>

using namespace std;

class Solution {
public:
  void createFactorialTable(int n) {
    factorialTable_.reserve(n + 1);
    factorialTable_.emplace_back(1);
    for (size_t i = 1; i <= n; ++i) { factorialTable_[i] = factorialTable_[i - 1] * i; }
  }

  uint64_t factorial(int n) const { return factorialTable_[n]; }

  void     createAvailableNumbers(int n) {
    availableNumbers_.clear();
    availableNumbers_.reserve(n);
    for (size_t i = 1; i <= n; ++i) { availableNumbers_.emplace_back('0' + i); }
  }

  void addIterationToResult(int n, int k) {
    if (k == 1) {
      for (auto n : availableNumbers_) { outputSequence_ += n; }
      return;
    }
    --k;
    const int permutationIndex  = k / factorial(n - 1);
    outputSequence_            += availableNumbers_[permutationIndex];
    availableNumbers_.erase(availableNumbers_.begin() + permutationIndex);
    k = 1 + (k % factorial(n - 1));
    addIterationToResult(n - 1, k);
  }

  string getPermutation(int n, int k) {
    // cout << "-------\n";
    createFactorialTable(n);
    createAvailableNumbers(n);
    addIterationToResult(n, k);
    return outputSequence_;
  }

  vector<uint64_t> factorialTable_;
  vector<char>     availableNumbers_;
  string           outputSequence_;
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
