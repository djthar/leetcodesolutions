#include <gtest/gtest.h>

#include <unordered_set>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<int, int, string>> {
protected:
  void SetUp() override {
    n        = std::get<0>(GetParam());
    k        = std::get<1>(GetParam());
    expected = std::get<2>(GetParam());
  }

  Solution solution;
  int      n = 0;
  int      k = 0;
  string   expected;
  string   result;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.getPermutation(n, k);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(Tests, SUITE_NAME,
                        ::testing::Values(std::make_tuple(3, 3, "213"),
                                          std::make_tuple(4, 9, "2314"),
                                          std::make_tuple(3, 1, "123")));
