#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <algorithm>
#include <unordered_set>
#include <vector>

using namespace std;

class Solution {
  using Key = vector<int>;

  struct KeyHasher {
    std::size_t operator()(const Key& vec) const {
      std::size_t seed = vec.size();
      for (auto& i : vec) { seed ^= i + 0x9e3779b9 + (seed << 6) + (seed >> 2); }
      return seed;
    }
  };

  using UniqueResultContainer = unordered_set<Key, KeyHasher>;

  void permuteUniqueRecursive(vector<int>&& nums, vector<int>&& prefix = {}) {
    if (nums.size() == 1
        or std::all_of(nums.begin() + 1, nums.end(), [&](int r) { return r == nums.front(); })) {
      std::copy(nums.begin(), nums.end(), back_inserter(prefix));
      result_.emplace(std::move(prefix));
      return;
    }
    for (size_t i = 0; i < nums.size(); ++i) {
      const int actualNum = nums[i];
      Key       numsCopyWithoutActualNum;
      numsCopyWithoutActualNum.reserve(nums.size());
      for (size_t j = 0; j < nums.size(); ++j) {
        if (j != i) { numsCopyWithoutActualNum.emplace_back(nums[j]); }
      }
      Key nextPrefix;
      nextPrefix.reserve(permutationsSize_);
      nextPrefix = prefix;
      nextPrefix.emplace_back(actualNum);
      permuteUniqueRecursive(std::move(numsCopyWithoutActualNum), std::move(nextPrefix));
    }
  }

public:
  vector<vector<int>> permuteUnique(vector<int>& nums) {
    permutationsSize_ = nums.size();
    permuteUniqueRecursive(std::move(nums));
    vector<Key> result;
    result.reserve(result_.size());
    while (not result_.empty()) {
      auto nh = result_.extract(result_.begin());
      result.emplace_back(std::move(nh.value()));
    }
    return result;
  }

private:
  size_t                permutationsSize_;
  UniqueResultContainer result_;
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
