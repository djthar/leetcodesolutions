#include <gtest/gtest.h>

#include <unordered_set>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<vector<int>, vector<vector<int>>>> {
protected:
  void SetUp() override {
    input    = std::get<0>(GetParam());
    expected = std::get<1>(GetParam());
  }

  Solution            solution;
  vector<int>         input;
  vector<vector<int>> expected;
  vector<vector<int>> result;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.permuteUnique(input);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(
    Tests, SUITE_NAME,
    ::testing::Values(
        std::make_tuple(vector<int>({
                          1, 1, 2
}),
                        vector<vector<int>>({{2, 1, 1}, {1, 2, 1}, {1, 1, 2}})),
        std::make_tuple(vector<int>({1, 2, 3}),
                        vector<vector<int>>(
                            {{3, 2, 1}, {2, 1, 3}, {3, 1, 2}, {2, 3, 1}, {1, 3, 2}, {1, 2, 3}}))));
