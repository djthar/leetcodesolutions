#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <algorithm>
#include <vector>

using namespace std;

class Solution {
public:
  vector<vector<int>> permute(vector<int>& nums) {
    if (nums.size() == 1) { return vector<vector<int>>(1, nums); }
    vector<vector<int>> result;
    for (size_t i = 0; i < nums.size(); ++i) {
      const int        actualNum = nums[i];
      vector<int>      numsCopyWithoutActualNum;
      std::vector<int> from_vector(10);
      copy_if(nums.begin(), nums.end(), back_inserter(numsCopyWithoutActualNum),
              [=](int x) { return x != actualNum; });
      auto permutations = permute(numsCopyWithoutActualNum);
      for (auto& permutation : permutations) {
        permutation.emplace_back(actualNum);
        result.emplace_back(std::move(permutation));
      }
    }
    return result;
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
