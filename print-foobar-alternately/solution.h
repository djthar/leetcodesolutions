#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <functional>
#include <mutex>

using namespace std;

class FooBar {
private:
  int        n;
  std::mutex foo_;
  std::mutex bar_;

public:
  FooBar(int n) {
    this->n = n;
    bar_.lock();
  }

  void foo(function<void()> printFoo) {
    while (true) {
      foo_.lock();
      --n;

      // printFoo() outputs "foo". Do not change or remove this line.
      printFoo();
      if (0 == n) {
        bar_.unlock();
        return;
      }
      bar_.unlock();
    }
  }

  void bar(function<void()> printBar) {
    while (true) {
      bar_.lock();

      // printBar() outputs "bar". Do not change or remove this line.
      printBar();
      if (0 == n) {
        foo_.unlock();
        return;
      }
      foo_.unlock();
    }
  }
};

using Solution = FooBar;

#endif  // LEETCODEEXERCISES_SOLUTION_H
