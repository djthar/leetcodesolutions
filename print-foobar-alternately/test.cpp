#include <gtest/gtest.h>

#include <future>
#include <unordered_set>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<int, string>> {
protected:
  void SetUp() override {
    input    = std::get<0>(GetParam());
    expected = std::get<1>(GetParam());
    solution = std::make_unique<Solution>(input);
  }

  std::unique_ptr<Solution> solution;
  int                       input;
  string                    expected;
  string                    result;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  const auto        printFoo = [this]() { result.append("foo"); };
  const auto        printBar = [this]() { result.append("bar"); };
  std::future<void> thread1
      = std::async(std::launch::async, [this, &printFoo]() { solution->foo(printFoo); });
  std::future<void> thread2
      = std::async(std::launch::async, [this, &printBar]() { solution->bar(printBar); });
  thread1.wait();
  thread2.wait();
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(Tests, SUITE_NAME,
                        ::testing::Values(std::make_tuple(5, "foobarfoobarfoobarfoobarfoobar")));
