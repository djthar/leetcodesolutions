#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <functional>
#include <semaphore>

using namespace std;

class ZeroEvenOdd {
public:
  ZeroEvenOdd(int n) {
    this->n = n;
    if (n % 2 == 0) {
      lastOdd  = n - 1;
      lastEven = n;
    } else {
      lastOdd  = n;
      lastEven = n - 1;
    }
  }

  // printNumber(x) outputs "x", where x is an integer.
  void zero(const function<void(int)>& printNumber) {
    while (true) {
      zeroSmph.acquire();
      const int actualIteration = iteration + 1;
      printNumber(0);
      releaseOddEven();
      if (actualIteration == n) { return; }
    }
  }

  void even(const function<void(int)>& printNumber) { number(printNumber, lastEven, evenSmph); }

  void odd(const function<void(int)>& printNumber) { number(printNumber, lastOdd, oddSmph); }

private:
  inline void number(const function<void(int)>& printNumber, int last, binary_semaphore& smph) {
    while (iteration < last) {
      smph.acquire();
      printNumber(++iteration);
      zeroSmph.release();
    }
  }

  inline void releaseOddEven() {
    if (iteration % 2 == 1) {
      evenSmph.release();
    } else {
      oddSmph.release();
    }
  }

  int              n;
  int              iteration = 0;
  int              lastOdd;
  int              lastEven;
  binary_semaphore zeroSmph{1};
  binary_semaphore oddSmph{0};
  binary_semaphore evenSmph{0};
};

using Solution = ZeroEvenOdd;

#endif  // LEETCODEEXERCISES_SOLUTION_H
