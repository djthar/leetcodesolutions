#include <gtest/gtest.h>

#include <future>
#include <unordered_set>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<int, string>> {
protected:
  void SetUp() override {
    input    = std::get<0>(GetParam());
    expected = std::get<1>(GetParam());
    solution = std::make_unique<Solution>(input);
  }

  std::unique_ptr<Solution> solution;
  int                       input;
  string                    expected;
  string                    result;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  const auto        printNumber = [this](int number) { result.append(to_string(number)); };
  std::future<void> thread1     = std::async(std::launch::async, [this, &printNumber]() {
    solution->zero([&printNumber](int number) {
      EXPECT_EQ(number, 0);
      printNumber(number);
    });
  });
  std::future<void> thread2     = std::async(std::launch::async, [this, &printNumber]() {
    solution->odd([&printNumber](int number) {
      EXPECT_EQ(number % 2, 1);
      printNumber(number);
    });
  });
  std::future<void> thread3     = std::async(std::launch::async, [this, &printNumber]() {
    solution->even([&printNumber](int number) {
      EXPECT_NE(number, 0);
      EXPECT_EQ(number % 2, 0);
      printNumber(number);
    });
  });
  thread1.wait();
  thread2.wait();
  thread3.wait();
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(Tests, SUITE_NAME, ::testing::Values(std::make_tuple(5, "0102030405")));
