#include <array>
#include <iostream>
#include <string>
#include <unordered_map>

static std::array<std::pair<const char*, std::unordered_map<std::string, int>>, 2> test{
  {{"uno", {}}, {"dos", {}}}
};

int main() {
  for (const auto& x : test) { std::cout << x.first << " " << x.second.size() << std::endl; }
  return 0;
}
