#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <cstdlib>
#include <iostream>
#include <unordered_set>
#include <vector>
using namespace std;

class Solution {
public:
    vector<int> findRedundantConnection(vector<vector<int>>& edges) {
        vector<unordered_set<int>> nodes(edges.size());
        for (size_t i=0; i<nodes.size(); ++i) {
            nodes[i].emplace(i);
        }
        for (auto edge : edges) {
            const int n1 = edge[0]-1;
            const int n2 = edge[1]-1;
            auto [it1, inserted_n1] = nodes[n1].emplace(n2);
            auto [it2, inserted_n2] = nodes[n2].emplace(n1);
            //printNodes(nodes);
            if (!inserted_n1 and !inserted_n2) {
                return edge;
            }
            nodes[n1].merge(nodes[n2]);
            for (auto nodeIndex : nodes[n1]) {
                if (nodeIndex != n1) nodes[nodeIndex] = nodes[n1];
            }
            //printNodes(nodes);
            //cout << "----------------" << endl;
        }
        return {};
    }

    void printNodes(vector<unordered_set<int>>& nodes) {
        for (size_t i=0; i<nodes.size();++i) {
            cout << "[" << i+1 << "] -> ";
            const auto& node = nodes[i];
            for (auto neigbour : node) {
                cout << neigbour+1 << " ";
            }
            cout << endl;
        }
        cout << endl;
    }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
