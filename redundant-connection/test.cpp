#include <gtest/gtest.h>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<vector<vector<int>>, vector<int>>> {
protected:
  void SetUp() override {
    input    = std::get<0>(GetParam());
    expected = std::get<1>(GetParam());
  }

  Solution            solution;
  vector<vector<int>> input    = {};
  vector<int>         result   = {};
  vector<int>         expected = {};
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.findRedundantConnection(input);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(
    Tests, SUITE_NAME,
    ::testing::Values(std::make_tuple(
                          vector<vector<int>>{
                            {1, 2},
                            {1, 3},
                            {2, 3}
},
                          vector<int>{2, 3}),
                      std::make_tuple(vector<vector<int>>{{1, 2}, {2, 3}, {3, 4}, {1, 4}, {1, 5}},
                                      vector<int>{1, 4}),
                      std::make_tuple(vector<vector<int>>{{3, 4}, {1, 2}, {2, 4}, {3, 5}, {2, 5}},
                                      vector<int>{2, 5})));
