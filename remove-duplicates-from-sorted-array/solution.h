#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <algorithm>
#include <vector>

using namespace std;

class Solution {
public:
  int removeDuplicates(vector<int>& nums) {
    nums.erase(std::remove_if(nums.begin(), nums.end(),
                              [&nums](const int& value) {
                                const size_t index = &value - &nums.front();
                                return index > 0 && nums[index - 1] == value;
                              }),
               nums.end());
    return nums.size();
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
