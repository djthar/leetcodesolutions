#include <gtest/gtest.h>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<vector<int>, vector<int>>> {
protected:
  Solution solution;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  auto expected = std::get<1>(GetParam());
  auto input    = std::get<0>(GetParam());
  solution.removeDuplicates(input);
  ASSERT_EQ(expected, input);
}

INSTANTIATE_TEST_SUITE_P(
    Tests, SUITE_NAME,
    ::testing::Values(std::make_tuple(vector<int>({1, 1, 2}), vector<int>({1, 2})),
                      std::make_tuple(vector<int>({0, 0, 1, 1, 1, 2, 2, 3, 3, 4}),
                                      vector<int>({0, 1, 2, 3, 4}))));
