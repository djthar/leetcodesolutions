#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <cstddef>
using namespace std;

struct ListNode {
  int       val;
  ListNode* next;

  ListNode() : val(0), next(nullptr) {}

  ListNode(int x) : val(x), next(nullptr) {}

  ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:
  ListNode* removeNthFromEnd(ListNode* head, int n) {
    ListNode* nthNext = head;
    for (size_t i = 0; i <= n; ++i) {
      if (nthNext == nullptr) { return head->next; }
      nthNext = nthNext->next;
    }
    ListNode* nthPrev = head;
    while (nullptr != nthNext) {
      nthNext = nthNext->next;
      nthPrev = nthPrev->next;
    }
    ListNode* removedNode = nthPrev->next;
    nthPrev->next         = removedNode->next;
    removedNode->next     = nullptr;
    delete removedNode;
    return head;
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
