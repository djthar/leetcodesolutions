#include <gtest/gtest.h>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<vector<int>, int, vector<int>>> {
protected:
  static ListNode* createList(const vector<int>& v) {
    if (v.empty()) { return nullptr; }
    auto*     front      = new ListNode(v.front());
    ListNode* actualNode = front;
    for (auto it = v.begin() + 1; it != v.end(); ++it) {
      actualNode->next = new ListNode(*it);
      actualNode       = actualNode->next;
    }
    return front;
  }

  static void freeList(ListNode* l) {
    if (nullptr == l) { return; }
    ListNode* next = l->next;
    delete l;
    freeList(next);
  }

  static vector<int> createVector(ListNode* l) {
    vector<int> v;
    while (nullptr != l) {
      v.emplace_back(l->val);
      l = l->next;
    }
    return v;
  }

  static string vectorToString(const vector<int>& v) {
    string result;
    for (auto value : v) { result += to_string(value) + ", "; }
    if (!result.empty()) {
      result.pop_back();
      result.pop_back();
    }
    return result;
  }

  void SetUp() override {
    input       = createList(std::get<0>(GetParam()));
    removeIndex = std::get<1>(GetParam());
    expected    = std::get<2>(GetParam());
  }

  void TearDown() override {
    freeList(input);
    // result is not free because it has the same elements as input
  }

  Solution    solution;
  ListNode*   input       = nullptr;
  int         removeIndex = 0;
  ListNode*   result      = nullptr;
  vector<int> expected;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result       = solution.removeNthFromEnd(input, removeIndex);
  auto resultV = createVector(result);
  ASSERT_EQ(resultV.size(), expected.size())
      << "result and expected are of unequal length and have contents " << vectorToString(resultV)
      << " vs " << vectorToString(expected);
  for (int i = 0; i < resultV.size(); ++i) {
    EXPECT_EQ(resultV[i], expected[i]) << "result and expected differ at index " << i;
  }
}

INSTANTIATE_TEST_SUITE_P(
    Tests, SUITE_NAME,
    ::testing::Values(std::make_tuple(vector<int>({1, 2, 3, 4, 5}), 2, vector<int>({1, 2, 3, 5})),
                      std::make_tuple(vector<int>({1}), 1, vector<int>({})),
                      std::make_tuple(vector<int>({1, 2}), 1, vector<int>({1}))));
