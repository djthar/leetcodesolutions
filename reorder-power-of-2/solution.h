#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <algorithm>
#include <cstdint>
#include <iostream>
#include <string>
#include <unordered_set>

using namespace std;

inline size_t getReorderedNumberHash(long n) {
  string v = to_string(n);
  std::sort(v.begin(), v.end());
  return hash<string>{}(v);
}

inline std::unordered_set<size_t> generatePowerOf2Numbers() {
  std::unordered_set<size_t> powerOf2Numbers;
  uint32_t                   actualValue = 1;
  powerOf2Numbers.reserve(30);
  powerOf2Numbers.emplace(getReorderedNumberHash(1));
  for (size_t i = 1; i < 30; ++i) {
    actualValue *= 2;
    powerOf2Numbers.emplace(getReorderedNumberHash(actualValue));
  }
  return powerOf2Numbers;
}

class Solution {
public:
  bool reorderedPowerOf2(int n) {
    const auto powerOf2Numbers = generatePowerOf2Numbers();
    for (const auto& num : powerOf2Numbers) { cout << num << " "; }
    cout << endl;
    const size_t reorderedNumberHash = getReorderedNumberHash(n);
    cout << reorderedNumberHash << endl;
    return powerOf2Numbers.count(reorderedNumberHash) == 1;
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
