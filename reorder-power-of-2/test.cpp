#include <gtest/gtest.h>

#include <unordered_set>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<int, bool>> {
protected:
  void SetUp() override {
    n        = std::get<0>(GetParam());
    expected = std::get<1>(GetParam());
  }

  Solution solution;
  int      n;
  bool     expected;
  bool     result;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.reorderedPowerOf2(n);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(Tests, SUITE_NAME,
                        ::testing::Values(std::make_tuple(1, true), std::make_tuple(10, false)));
