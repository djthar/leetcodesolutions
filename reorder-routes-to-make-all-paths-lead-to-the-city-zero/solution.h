#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <deque>
#include <unordered_set>
#include <vector>

using namespace std;

class Node {
public:
  std::unordered_set<int> outgoing;
  std::unordered_set<int> ingoing;
  bool                    visited = false;
};

class Solution {
public:
  int minReorder(int n, vector<vector<int>>& connections) {
    nodes_.resize(n);
    for (const auto& connection : connections) {
      const int outgoingNodeIndex = connection[0];
      Node&     outgoingNode      = nodes_[outgoingNodeIndex];
      const int ingoingNodeIndex  = connection[1];
      Node&     ingoingNode       = nodes_[ingoingNodeIndex];
      outgoingNode.outgoing.emplace(ingoingNodeIndex);
      ingoingNode.ingoing.emplace(outgoingNodeIndex);
    }

    visit(nodes_[0]);

    return changes;
  }

private:
  void visit(Node& node) {
    node.visited = true;
    for (int childIdx : node.outgoing) {
      Node& childNode = nodes_[childIdx];
      if (!childNode.visited) {
        ++changes;
        visit(childNode);
      }
    }
    for (int childIdx : node.ingoing) {
      Node& childNode = nodes_[childIdx];
      if (!childNode.visited) { visit(childNode); }
    }
  }

  void visitBfs(Node& node) {
    node.visited = true;
    deque<Node*> nodes;
    nodes.emplace_back(&node);
    while (!nodes.empty()) {
      auto pNode = nodes.front();
      nodes.pop_front();
      for (auto childIdx : pNode->outgoing) {
        Node& childNode = nodes_[childIdx];
        if (!childNode.visited) {
          nodes.push_back(&childNode);
          childNode.visited = true;
        }
      }
    }
  }

  std::vector<Node> nodes_;
  int               changes = 0;
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
