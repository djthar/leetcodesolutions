#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <cstdlib>
#include <string>
using namespace std;

class Solution {
public:
  int reverse(int x) {
    bool isNegative   = x < 0;
    x                 = abs(x);
    auto str          = std::to_string(x);
    int  numberResult = 0;
    int  exponent     = 1;
    if (str.size() > 10) { return 0; }
    for (auto c : str) {
      int currentDigit = c - '0';
      if (numberResult > 147483647 and currentDigit >= 2) { return 0; }
      numberResult += currentDigit * exponent;
      exponent      = exponent < 1000000000 ? exponent * 10 : 0;
    }
    return isNegative ? -numberResult : numberResult;
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
