#include <gtest/gtest.h>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<int, int>> {
protected:
  void SetUp() override {
    input    = std::get<0>(GetParam());
    expected = std::get<1>(GetParam());
  }

  Solution solution;
  int      input    = 0;
  int      result   = 0;
  int      expected = 0;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.reverse(input);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(Tests, SUITE_NAME,
                         ::testing::Values(std::make_tuple(123, 321), std::make_tuple(-123, -321),
                                           std::make_tuple(0, 0), std::make_tuple(120, 21)));
