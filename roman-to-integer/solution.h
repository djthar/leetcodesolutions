#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <string>
#include <unordered_map>

using namespace std;

class Solution {
public:
  int romanToInt(string s) {
    int          result = 0;
    const size_t sSize  = s.size() - 1;
    for (size_t i = 0; i <= sSize; ++i) {
      const int currentValue = toVal[s[i]];
      if (i <= sSize - 1 and toVal[s[i + 1]] > currentValue) {
        result += toVal[s[i + 1]] - currentValue;
        ++i;
        continue;
      }
      result += currentValue;
    }
    return result;
  }

  std::unordered_map<char, int> toVal = {
    {'I',    1},
    {'V',    5},
    {'X',   10},
    {'L',   50},
    {'C',  100},
    {'D',  500},
    {'M', 1000}
  };
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
