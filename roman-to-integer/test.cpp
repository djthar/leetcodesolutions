#include <gtest/gtest.h>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<string, int>> {
protected:
  void SetUp() override {
    input    = std::get<0>(GetParam());
    expected = std::get<1>(GetParam());
  }

  Solution solution;
  string   input    = {};
  int      expected = {};
  int      result   = {};
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.romanToInt(input);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(Tests, SUITE_NAME,
                         ::testing::Values(std::make_tuple("III", 3), std::make_tuple("LVIII", 58),
                                           std::make_tuple("MCMXCIV", 1994)));
