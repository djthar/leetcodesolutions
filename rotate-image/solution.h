#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

class Solution {
  using Coordinate = tuple<size_t, size_t>;

public:
  void printMatrix() const {
    for (const auto& row : *matrix_) {
      for (auto v : row) { cout << v << " "; }
      cout << "\n";
    }
    cout << "-----" << endl;
  }

  void rotateRow(Coordinate initialPoint, size_t nPoints) {
    // cout << "rotate from " << get<0>(initialPoint) << "," << get<1>(initialPoint) << " a total of
    // " << nPoints << endl;
    if (0 == nPoints) { return; }
    for (size_t i = 0; i < nPoints; ++i) {
      rotatePoint({get<0>(initialPoint), get<1>(initialPoint) + i});
    }
  }

  void rotatePoint(Coordinate point) {
    int  pointValue = (*matrix_)[get<0>(point)][get<1>(point)];
    auto nextPoint  = getRotatedPoint(point);
    while (nextPoint != point) {
      const int temporalPointValue = (*matrix_)[get<0>(nextPoint)][get<1>(nextPoint)];
      (*matrix_)[get<0>(nextPoint)][get<1>(nextPoint)] = pointValue;
      nextPoint                                        = getRotatedPoint(nextPoint);
      pointValue                                       = temporalPointValue;
    }
    (*matrix_)[get<0>(point)][get<1>(point)] = pointValue;
  }

  void rotate(vector<vector<int>>& matrix) {
    matrixSize = matrix.size();
    if (matrixSize == 1) { return; }
    matrix_ = &matrix;
    // printMatrix();
    for (size_t i = 0; i <= (matrixSize / 2 - 1); ++i) {
      rotateRow({i, i}, matrixSize - 1 - 2 * i);
      // printMatrix();
    }
  }

private:
  Coordinate getRotatedPoint(const Coordinate& point) const {
    // cout << get<0>(point) << "," << get<1>(point) << " to ";
    // cout << get<1>(point) << "," << matrixSize - 1 - get<0>(point) << endl;
    return {get<1>(point), matrixSize - 1 - get<0>(point)};
  }

  size_t               matrixSize = 0;
  vector<vector<int>>* matrix_{nullptr};
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
