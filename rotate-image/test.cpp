#include <gtest/gtest.h>

#include <unordered_set>

#include "solution.h"

class SUITE_NAME :
    public ::testing::TestWithParam<std::tuple<vector<vector<int>>, vector<vector<int>>>> {
protected:
  void SetUp() override {
    input    = std::get<0>(GetParam());
    expected = std::get<1>(GetParam());
  }

  Solution            solution;
  vector<vector<int>> input;
  vector<vector<int>> expected;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  solution.rotate(input);
  EXPECT_EQ(input, expected);
}

INSTANTIATE_TEST_SUITE_P(
    Tests, SUITE_NAME,
    ::testing::Values(
        std::make_tuple(vector<vector<int>>({
                          {1, 2, 3},
                          {4, 5, 6},
                          {7, 8, 9}
}),
                        vector<vector<int>>({{7, 4, 1}, {8, 5, 2}, {9, 6, 3}})),
        std::make_tuple(
            vector<vector<int>>({{5, 1, 9, 11}, {2, 4, 8, 10}, {13, 3, 6, 7}, {15, 14, 12, 16}}),
            vector<vector<int>>({{15, 13, 2, 5}, {14, 3, 4, 1}, {12, 6, 8, 9}, {16, 7, 10, 11}})),
        std::make_tuple(vector<vector<int>>({{1}}), vector<vector<int>>({{1}})),
        std::make_tuple(vector<vector<int>>({{1, 2}, {3, 4}}),
                        vector<vector<int>>({{3, 1}, {4, 2}}))));
