#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <algorithm>
#include <vector>

using namespace std;

class Solution {
  static size_t getMiddlePoint(const tuple<size_t, size_t>& range) {
    return (get<0>(range) + get<1>(range)) / 2;
  }

  static void moveRightSide(tuple<size_t, size_t>& range) {
    const auto middlePoint = getMiddlePoint(range);
    get<1>(range)          = middlePoint;
    if (get<1>(range) > get<0>(range)) { --get<1>(range); }
  }

  static void moveLeftSide(tuple<size_t, size_t>& range) {
    const auto middlePoint = getMiddlePoint(range);
    get<0>(range)          = middlePoint;
    if (get<0>(range) < get<1>(range)) { ++get<0>(range); }
  }

public:
  bool search(vector<int>& nums, int target) {
    switch (nums.size()) {
      case 0 :
        return false;
      case 1 :
      case 2 :
        return target == nums.front() or target == nums.back();
      default :
        if (target == nums.front() or target == nums.back()) { return true; }
        break;
    }

    tuple<size_t, size_t> actualRange = {0, nums.size() - 1};
    while (get<0>(actualRange) <= get<1>(actualRange)) {
      auto& leftIndex  = get<0>(actualRange);
      auto& rightIndex = get<1>(actualRange);
      while (leftIndex < rightIndex and leftIndex < nums.size() - 1
             and (nums[leftIndex] == nums[leftIndex + 1] or nums[leftIndex] == nums[rightIndex])) {
        ++leftIndex;
      }
      while (leftIndex < rightIndex and nums[rightIndex] == nums[rightIndex - 1]) { --rightIndex; }
      const int leftValue  = nums[leftIndex];
      const int rightValue = nums[rightIndex];
      if (leftValue < rightValue) {
        return search(nums.begin() + leftIndex, nums.begin() + rightIndex, target);
      }
      size_t    actualIndex = getMiddlePoint(actualRange);
      const int actualValue = nums[actualIndex];
      if (actualValue == target or leftValue == target or rightValue == target) { return true; }
      if (leftIndex == rightIndex) { return false; }
      if (actualValue > leftValue) {
        if (actualValue < target and target < rightValue) {
          return search(nums.begin() + actualIndex, nums.begin() + rightIndex, target);
        }
        moveLeftSide(actualRange);
      } else {
        if (actualValue > target and target > leftValue) {
          return search(nums.begin() + leftIndex, nums.begin() + actualIndex, target);
        }
        moveRightSide(actualRange);
      }
    }
    return false;
  }

  static bool search(const vector<int>::const_iterator& left,
                     const vector<int>::const_iterator& right, int target) {
    auto targetIt = lower_bound(left, right, target);
    if (targetIt == right) { return false; }
    return *targetIt == target;
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
