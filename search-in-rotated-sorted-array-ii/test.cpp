#include <gtest/gtest.h>

#include <unordered_set>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<vector<int>, int, bool>> {
protected:
  void SetUp() override {
    input    = std::get<0>(GetParam());
    target   = std::get<1>(GetParam());
    expected = std::get<2>(GetParam());
  }

  Solution    solution;
  vector<int> input;
  int         target   = 0;
  int         expected = 0;
  int         result   = 0;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.search(input, target);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(
    Tests, SUITE_NAME,
    ::testing::Values(std::make_tuple(vector<int>({2, 5, 6, 0, 0, 1, 2}), 0, true),
                      std::make_tuple(vector<int>({2, 5, 6, 0, 0, 1, 2}), 3, false),
                      std::make_tuple(vector<int>({2, 2, 2, 2, 2, 2, 2}), 3, false),
                      std::make_tuple(vector<int>({2, 2, 2, 2, 2, 2, 2}), 2, true),
                      std::make_tuple(vector<int>({2, 2, 2, 2, 2, 3, 2}), 3, true),
                      std::make_tuple(vector<int>({1, 0, 1, 1, 1}), 0, true),
                      std::make_tuple(vector<int>({1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1,
                                                   1, 1, 1}),
                                      2, true),
                      std::make_tuple(vector<int>({3, 1, 1}), 0, false),
                      std::make_tuple(vector<int>({4, 5, 6, 7, 0, 1, 2}), 1, true),
                      std::make_tuple(vector<int>({4, 5, 6, 7, 0, 1, 2}), 5, true),
                      std::make_tuple(vector<int>({1}), 0, false)));
