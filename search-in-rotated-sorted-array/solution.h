#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

class Solution {
  static size_t getMiddlePoint(const tuple<size_t, size_t>& range) {
    return (get<0>(range) + get<1>(range)) / 2;
  }

  template<unsigned int N>
  void moveRangeSide(tuple<size_t, size_t>& range) {
    get<N>(range) = getMiddlePoint(range);
  }

  void        moveRightSide(tuple<size_t, size_t>& range) { moveRangeSide<1>(range); }

  void        moveLeftSide(tuple<size_t, size_t>& range) { moveRangeSide<0>(range); }

  static void print_v(vector<int>::iterator v_begin, vector<int>::iterator v_end) {
    while (v_begin != v_end) {
      cout << *v_begin << " ";
      ++v_begin;
    }
    cout << endl;
  }

  size_t searchMaxValue(vector<int>& nums) {
    const int rightValue = nums.back();
    const int leftValue  = nums.front();
    if (rightValue >= leftValue) {
      return nums.size() - 1;
    } else if (nums.size() == 2) {
      return 0;
    }
    tuple<size_t, size_t> actualRange = {0, nums.size() - 1};
    size_t                actualIndex = getMiddlePoint(actualRange);
    while (nums[actualIndex] < nums[actualIndex + 1]) {
      const int actualValue = nums[actualIndex];
      if (actualValue > nums[get<0>(actualRange)]) {
        moveLeftSide(actualRange);
      } else {
        moveRightSide(actualRange);
      }
      actualIndex = getMiddlePoint(actualRange);
    }
    return actualIndex;
  }

public:
  int search(vector<int>& nums, int target) {
    const size_t maxValueIndex = searchMaxValue(nums);
    if (target == nums[maxValueIndex]) {
      // cout << "threshold is max value\n";
      return static_cast<int>(maxValueIndex);
    } else if (target > nums[maxValueIndex]) {
      // cout << "threshold is greater than max value\n";
      return -1;
    }
    auto targetItBegin = nums.begin();
    auto targetItEnd   = nums.end();
    if (target >= nums.front()) {
      // cout << "change end\n";
      targetItEnd = nums.begin() + static_cast<int>(maxValueIndex);
    } else if (target <= nums.back()) {
      // cout << "change begin\n";
      targetItBegin = nums.begin() + static_cast<int>(maxValueIndex) + 1;
    } else {
      // cout << "threshold between first and back values\n";
      return -1;
    }
    // print_v(targetItBegin, targetItEnd);
    auto targetIt = lower_bound(targetItBegin, targetItEnd, target);
    if (targetIt == targetItEnd) {
      // cout << "Iterator at end\n";
      return -1;
    }
    /*
    if (*targetIt != threshold) {
        cout << *targetIt << " vs " << threshold << endl;
    }
    */
    return *targetIt == target ? static_cast<int>(targetIt - nums.begin()) : -1;
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
