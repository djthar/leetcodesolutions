#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <deque>
#include <vector>

using namespace std;

class Solution {
public:
  vector<int> maxSlidingWindow(vector<int>& nums, int k) {
    deque<int>  deq;
    vector<int> result;
    result.reserve(nums.size() - k + 1);
    int left  = 0;
    int right = 0;
    while (right < nums.size()) {
      while (!deq.empty() && nums[right] > nums[deq.back()]) { deq.pop_back(); }
      deq.push_back(right);
      if (left > deq.front()) { deq.pop_front(); }
      if ((right + 1) >= k) {
        result.emplace_back(nums[deq.front()]);
        ++left;
      }
      ++right;
    }
    return result;
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
