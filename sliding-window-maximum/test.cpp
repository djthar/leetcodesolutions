#include <gtest/gtest.h>

#include "solution.h"

using InputType  = std::tuple<std::vector<int>, int>;
using OutputType = std::vector<int>;

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<InputType, OutputType>> {
protected:
  void SetUp() override {
    input    = std::get<0>(GetParam());
    expected = std::get<1>(GetParam());
  }

  Solution   solution;
  InputType  input;
  OutputType expected;
  OutputType result;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.maxSlidingWindow(get<0>(input), get<1>(input));
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(Tests, SUITE_NAME,
                        ::testing::Values(std::make_tuple(
                                              InputType{
                                                {1, 3, -1, -3, 5, 3, 6, 7},
                                                3
},
                                              OutputType{3, 3, 5, 5, 6, 7}),
                                          std::make_tuple(InputType{{1}, 1}, OutputType{1})));
