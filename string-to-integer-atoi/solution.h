#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <limits>
#include <string>

using namespace std;

class Solution {
public:
  int myAtoi(string s) {
    const char* myData           = skipSpaces(s.c_str());
    bool        isNegative       = false;
    std::tie(myData, isNegative) = skipAndGetSign(myData);
    myData                       = skipZeros(myData);
    const size_t skippedChars    = myData - s.c_str();
    auto         numberString    = getNumberString(string_view(myData, s.size() - skippedChars));
    int          numberResult    = 0;
    int          exponent        = 1;
    if (numberString.size() > 10) { return getMaxReturnValue(isNegative); }
    for (auto cIt = numberString.rbegin(); cIt != numberString.rend(); ++cIt) {
      int currentDigit = *cIt - '0';
      if (numberResult > 147483647 and currentDigit >= 2) { return getMaxReturnValue(isNegative); }
      numberResult += currentDigit * exponent;
      exponent      = exponent < 1000000000 ? exponent * 10 : 0;
    }
    return isNegative ? -numberResult : numberResult;
  }

private:
  const char* skipSpaces(const char* s) {
    while (*s == ' ') { ++s; }
    return s;
  }

  const char* skipZeros(const char* s) {
    while (*s == '0') { ++s; }
    return s;
  }

  std::tuple<const char*, bool> skipAndGetSign(const char* s) {
    bool isNegative = false;
    if ('-' == *s) {
      isNegative = true;
      ++s;
    } else if ('+' == *s) {
      ++s;
    }
    return {s, isNegative};
  }

  std::string_view getNumberString(string_view s) {
    size_t length = 0;
    for (auto c : s) {
      if (c >= '0' and c <= '9') {
        ++length;
      } else {
        break;
      }
    }
    return {s.data(), length};
  }

  int getMaxReturnValue(bool isNegative) {
    return isNegative ? numeric_limits<int>::min() : numeric_limits<int>::max();
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
