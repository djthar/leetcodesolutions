#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <algorithm>
#include <limits>
#include <vector>

using namespace std;

class Solution {
public:
  vector<int> successfulPairs(vector<int>& spells, vector<int>& potions, long long success) {
    std::sort(potions.begin(), potions.end());
    vector<int> result(spells.size(), 0);
    for (size_t i = 0; i < spells.size(); ++i) {
      const auto minValue = 1 + ((success - 1) / spells[i]);
      result[i]           = numeric_limits<int>::max() < minValue
                              ? 0
                              : static_cast<int>(potions.end()
                                                 - lower_bound(potions.begin(), potions.end(),
                                                               static_cast<int>(minValue)));
    }
    return result;
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
