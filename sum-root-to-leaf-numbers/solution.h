#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

using namespace std;

struct TreeNode {
  int       val;
  TreeNode* left;
  TreeNode* right;

  TreeNode() : val(0), left(nullptr), right(nullptr) {}

  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}

  TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};

class Solution {
public:
  int sumNumbers(TreeNode* root) {
    if (nullptr == root) { return 0; }
    sumNumbersRecursive(root, 0);
    return result_;
  }

private:
  void sumNumbersRecursive(TreeNode* node, int actualCount) {
    actualCount = actualCount * 10 + node->val;
    if (nullptr == node->left and nullptr == node->right) {
      result_ += actualCount;
      return;
    }
    if (nullptr != node->left) { sumNumbersRecursive(node->left, actualCount); }
    if (nullptr != node->right) { sumNumbersRecursive(node->right, actualCount); }
  }

  int result_ = 0;
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
