#include <gtest/gtest.h>

#include <functional>
#include <queue>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<vector<optional<int>>, int>> {
protected:
  void SetUp() override {
    input    = getTree(std::get<0>(GetParam()));
    expected = std::get<1>(GetParam());
  }

  void TearDown() override { freeTree(input); }

  void freeTree(TreeNode*& node) {
    if (nullptr == node) { return; }
    freeTree(node->left);
    freeTree(node->right);
    delete node;
    node = nullptr;
  }

  static TreeNode* getTree(const vector<optional<int>>& tree) {
    if (tree.empty() or not tree.front()) { return nullptr; }
    if (tree.size() == 1) { return new TreeNode(tree.front().value()); }
    if (0 != ((tree.size() - 1) % 2)) { return nullptr; }
    auto             treeIt = tree.begin();
    auto*            root   = new TreeNode(treeIt->value());
    queue<TreeNode*> nodesQueue;
    nodesQueue.push(root);
    while (not nodesQueue.empty()) {
      TreeNode* node = nodesQueue.front();
      nodesQueue.pop();
      for (TreeNode** n : {&node->left, &node->right}) {
        if (treeIt != tree.end()) {
          ++treeIt;
          *n = *treeIt ? new TreeNode(treeIt->value()) : nullptr;
          if (nullptr != *n) { nodesQueue.push(*n); }
        }
      }
    }
    return root;
  }

  Solution  solution;
  TreeNode* input    = nullptr;
  int       expected = 0;
  int       result   = 0;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.sumNumbers(input);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(Tests, SUITE_NAME,
                        ::testing::Values(std::make_tuple(vector<optional<int>>({1, 2, 3}), 25),
                                          std::make_tuple(vector<optional<int>>({4, 9, 0, 5, 1}),
                                                          1026)));
