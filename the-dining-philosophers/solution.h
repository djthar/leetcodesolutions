#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <functional>
#include <semaphore>

using namespace std;

class DiningPhilosophers {
public:
  DiningPhilosophers() = default;

  void wantsToEat(int philosopher, const function<void()>& pickLeftFork,
                  const function<void()>& pickRightFork, const function<void()>& eat,
                  const function<void()>& putLeftFork, const function<void()>& putRightFork) {
    semaphore_.acquire();
    const std::lock_guard<std::mutex> leftForkGuard(getLeftForkMutex(philosopher));
    pickLeftFork();
    {
      const std::lock_guard<std::mutex> rightForkGuard(getRightForkMutex(philosopher));
      pickRightFork();
      eat();
      putRightFork();
    }
    putLeftFork();
    semaphore_.release();
  }

  static constexpr int N_PHILOSOPHER = 5;

private:
  static inline int getLeftForkIndex(int philosopher) { return philosopher; }

  static inline int getRightForkIndex(int philosopher) {
    return (philosopher + (N_PHILOSOPHER - 1)) % N_PHILOSOPHER;
  }

  inline std::mutex& getLeftForkMutex(int philosopher) {
    return forks_[getLeftForkIndex(philosopher)];
  }

  inline std::mutex& getRightForkMutex(int philosopher) {
    return forks_[getRightForkIndex(philosopher)];
  }

  std::array<std::mutex, N_PHILOSOPHER>      forks_;
  std::counting_semaphore<N_PHILOSOPHER - 1> semaphore_{N_PHILOSOPHER - 1};
};

using Solution = DiningPhilosophers;

#endif  // LEETCODEEXERCISES_SOLUTION_H
