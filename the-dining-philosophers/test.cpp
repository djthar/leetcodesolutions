#include <gmock/gmock.h>

#include <future>
#include <random>
#include <thread>
#include <unordered_set>

#include "solution.h"

using ::testing::Expectation;

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<int>> {
protected:
  void SetUp() override {
    input    = std::get<0>(GetParam());
    solution = std::make_unique<Solution>();
  }

  void randomSleep() { std::this_thread::sleep_for(std::chrono::milliseconds(dis(gen))); }

  std::unique_ptr<Solution>       solution;
  int                             input;
  std::random_device              rd;
  std::mt19937                    gen{rd()};
  std::uniform_int_distribution<> dis{0, 100};
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  class PhilosopherMock {
  public:
    MOCK_METHOD(void, pickLeft, (int philosopher));
    MOCK_METHOD(void, pickRight, (int philosopher));
    MOCK_METHOD(void, putLeft, (int philosopher));
    MOCK_METHOD(void, putRight, (int philosopher));
    MOCK_METHOD(void, eat, (int philosopher));
  };

  PhilosopherMock                philosopherMock;
  std::vector<std::future<void>> philosophers;
  philosophers.reserve(Solution::N_PHILOSOPHER);
  for (int i = 0; i < Solution::N_PHILOSOPHER; ++i) {
    Expectation pickLeft  = EXPECT_CALL(philosopherMock, pickLeft(i)).Times(input);
    Expectation pickRight = EXPECT_CALL(philosopherMock, pickRight(i)).Times(input);
    Expectation eat = EXPECT_CALL(philosopherMock, eat(i)).Times(input).After(pickLeft, pickRight);
    EXPECT_CALL(philosopherMock, putLeft(i)).Times(input).After(eat);
    EXPECT_CALL(philosopherMock, putRight(i)).Times(input).After(eat);
    philosophers.emplace_back(std::async(std::launch::async, [i, &philosopherMock, this]() {
      for (size_t j = 0; j < input; ++j) {
        randomSleep();
        solution->wantsToEat(
            i, [&]() { philosopherMock.pickLeft(i); }, [&]() { philosopherMock.pickRight(i); },
            [&]() { philosopherMock.eat(i); }, [&]() { philosopherMock.putLeft(i); },
            [&]() { philosopherMock.putRight(i); });
      }
    }));
  }
  for (auto& philosopher : philosophers) { philosopher.wait(); }
}

INSTANTIATE_TEST_SUITE_P(Tests, SUITE_NAME, ::testing::Values(std::make_tuple(1)));
