#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <algorithm>
#include <array>
#include <vector>

using namespace std;

template<typename T, typename Pred>
typename std::vector<T>::iterator insert_sorted(std::vector<T>& vec, T const& item, Pred pred) {
  return vec.insert(std::upper_bound(vec.begin(), vec.end(), item, pred), item);
}

class Solution {
public:
  vector<int> kWeakestRows(vector<vector<int>>& mat, int k) {
    vector<array<int, 2>> sortedResult;
    sortedResult.reserve(mat.size());
    for (size_t i = 0; i < mat.size(); ++i) {
      const auto& v   = mat[i];
      int         sum = 0;
      auto        it  = v.begin();
      while (it != v.end() && *it == 1) {
        ++sum;
        ++it;
      }
      insert_sorted(
          sortedResult, {sum, static_cast<int>(i)},
          [](const array<int, 2>& lhs, const array<int, 2>& rhs) { return lhs[0] < rhs[0]; });
    }
    vector<int> result;
    result.reserve(k);
    for (size_t i = 0; i < k; ++i) { result.emplace_back(sortedResult[i][1]); }
    return result;
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
