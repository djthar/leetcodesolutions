#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <vector>

using namespace std;

class Solution {
public:
  int totalHammingDistance(vector<int>& nums) {
    if (nums.size() < 2) { return 0; }
    unsigned int distance = 0;
    const size_t numsSize = nums.size();
    for (size_t i = 0; i < MAX_BITS; ++i) {
      size_t endingOneCount = 0;
      for (auto& n : nums) {
        endingOneCount  += n & 0x1;
        n              >>= 1;
      }
      distance += (numsSize - endingOneCount) * endingOneCount;
    }
    return static_cast<int>(distance);
  }

private:
  constexpr static size_t MAX_BITS = 30;
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
