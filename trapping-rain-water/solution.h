#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <algorithm>
#include <vector>

using namespace std;

class Solution {
public:
  int trap(vector<int> height) {
    if (height.size() < 3) { return 0; }
    auto actualElevationIt = height.begin();
    int  result            = 0;
    while (actualElevationIt != height.end()) {
      auto nextMaxElementIt = max_element(actualElevationIt + 1, height.end());
      if (nextMaxElementIt == height.end()) {
        ++actualElevationIt;
        continue;
      }
      const int nextMaxHeight = min(*actualElevationIt, *nextMaxElementIt);
      auto      nextSectionIt = getNextSectionIt(nextMaxHeight, actualElevationIt, height);
      if (nextSectionIt != height.end()) {
        while (actualElevationIt != nextSectionIt) {
          if (*actualElevationIt < nextMaxHeight) { result += nextMaxHeight - *actualElevationIt; }
          ++actualElevationIt;
        }
      } else {
        ++actualElevationIt;
      }
    }
    return result;
  }

  static vector<int>::iterator getNextSectionIt(int                          maxHeight,
                                                const vector<int>::iterator& actualElevationIt,
                                                vector<int>&                 height) {
    return find_if(actualElevationIt + 1, height.end(),
                   [maxHeight](int h) { return h >= maxHeight; });
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
