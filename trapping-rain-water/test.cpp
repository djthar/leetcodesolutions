#include <gtest/gtest.h>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<vector<int>, int>> {
protected:
  void SetUp() override {
    input    = std::get<0>(GetParam());
    expected = std::get<1>(GetParam());
  }

  Solution         solution;
  std::vector<int> input;
  int              expected = 0;
  int              result   = 0;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.trap(input);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(
    Tests, SUITE_NAME,
    ::testing::Values(std::make_tuple(vector<int>({0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1}), 6),
                      std::make_tuple(vector<int>({4, 2, 0, 3, 2, 5}), 9)));
