#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <unordered_map>
#include <vector>

using namespace std;

class Solution {
public:
  vector<int> twoSum(vector<int>& nums, int target) {
    unordered_map<int, int> numbers;
    for (int i = 0; i < nums.size(); ++i) {
      const int targetNumber   = target - nums[i];
      auto      targetNumberIt = numbers.find(targetNumber);
      if (targetNumberIt != numbers.end()) { return {targetNumberIt->second, i}; }
      numbers.emplace(nums[i], i);
    }
    return {
      {0, 0}
    };
  }
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
