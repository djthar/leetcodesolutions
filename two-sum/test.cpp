#include <gtest/gtest.h>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<vector<int>, int, vector<int>>> {
protected:
  static string vectorToString(const vector<int>& v) {
    string result;
    for (auto value : v) { result += to_string(value) + ", "; }
    if (!result.empty()) {
      result.pop_back();
      result.pop_back();
    }
    return result;
  }

  void SetUp() override {
    input    = std::get<0>(GetParam());
    target   = std::get<1>(GetParam());
    expected = std::get<2>(GetParam());
  }

  Solution    solution;
  vector<int> input;
  int         target = 0;
  vector<int> result;
  vector<int> expected;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.twoSum(input, target);
  ASSERT_EQ(result.size(), expected.size())
      << "result and expected are of unequal length and have contents " << vectorToString(result)
      << " vs " << vectorToString(expected);
  for (int i = 0; i < result.size(); ++i) {
    EXPECT_EQ(result[i], expected[i])
        << "result and expected differ at index " << i << " and have contents "
        << vectorToString(result) << " vs " << vectorToString(expected);
  }
}

INSTANTIATE_TEST_SUITE_P(
    Tests, SUITE_NAME,
    ::testing::Values(std::make_tuple(vector<int>({2, 7, 11, 15}), 9, vector<int>({0, 1})),
                      std::make_tuple(vector<int>({3, 2, 4}), 6, vector<int>({1, 2})),
                      std::make_tuple(vector<int>({3, 3}), 6, vector<int>({0, 1}))));
