#ifndef LEETCODEEXERCISES_SOLUTION_H
#define LEETCODEEXERCISES_SOLUTION_H

#include <string>
#include <vector>

using namespace std;

class Solution {
public:
  string convert(string s, int numRows) {
    setModuloAndRows(numRows);
    vector<string> rows(numRows, "");
    for (size_t i = 0; i < s.size(); ++i) { rows[getRowFromIndex(i)] += s[i]; }
    string result = std::move(rows[0]);
    result.reserve(s.size());
    for (size_t i = 1; i < rows.size(); ++i) { result += rows[i]; }
    return result;
  }

private:
  size_t getRowFromIndex(size_t index) {
    size_t resultingIndex = index % modulo_;
    // cout << "modulo: " << index << " -> " << resultingIndex << endl;
    if (resultingIndex >= numRows_) {
      const size_t dToLastRow = resultingIndex - (numRows_ - 1);
      resultingIndex          = numRows_ - 1 - dToLastRow;
    }
    // cout << index << " -> " << resultingIndex << endl;
    return resultingIndex;
  }

  void setModuloAndRows(size_t numRows) {
    modulo_  = numRows < 3 ? numRows : 2 * numRows - 2;
    numRows_ = numRows;
  }

  size_t modulo_  = 1;
  size_t numRows_ = 1;
};

#endif  // LEETCODEEXERCISES_SOLUTION_H
