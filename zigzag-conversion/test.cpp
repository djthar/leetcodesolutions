#include <gtest/gtest.h>

#include "solution.h"

class SUITE_NAME : public ::testing::TestWithParam<std::tuple<string, int, string>> {
protected:
  void SetUp() override {
    input    = std::get<0>(GetParam());
    numRows  = std::get<1>(GetParam());
    expected = std::get<2>(GetParam());
  }

  Solution    solution;
  std::string input;
  int         numRows = 0;
  string      expected;
  string      result;
};

TEST_P(SUITE_NAME, CheckIfSolutionCorrect) {
  result = solution.convert(input, numRows);
  EXPECT_EQ(result, expected);
}

INSTANTIATE_TEST_SUITE_P(Tests, SUITE_NAME,
                        ::testing::Values(std::make_tuple("PAYPALISHIRING", 3, "PAHNAPLSIIGYIR"),
                                          std::make_tuple("PAYPALISHIRING", 4, "PINALSIGYAHRPI"),
                                          std::make_tuple("A", 1, "A")));
